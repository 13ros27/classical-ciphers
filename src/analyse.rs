#![allow(dead_code)]

use std::collections::HashMap;
use std::fs::File;
use std::io::{prelude::*, BufReader};

use crate::{modify::clean, pattern::Pattern};

pub const ASCII_LOWER: [char; 26] = [
    'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's',
    't', 'u', 'v', 'w', 'x', 'y', 'z',
];

pub const LETTER_FREQS: [f64; 26] = [
    8.55, 1.60, 3.16, 3.87, 12.10, 2.18, 2.09, 4.96, 7.33, 0.22, 0.81, 4.21, 2.53, 7.17, 7.47,
    2.07, 0.10, 6.33, 6.73, 8.94, 2.68, 1.06, 1.83, 0.19, 1.72, 0.11,
];

pub const MORSE_LETTERS: [&str; 26] = [
    ".-", "-...", "-.-.", "-..", ".", "..-.", "--.", "....", "..", ".---", "-.-", ".-..", "--",
    "-.", "---", ".--.", "--.-", ".-.", "...", "-", "..-", "...-", ".--", "-..-", "-.--", "--..",
];

/// Returns the number of instances of each letter in a message
pub fn count_letters(mes: &str) -> Vec<usize> {
    // TODO: More efficient to use builtin count?
    let mes = clean(&mes.to_ascii_uppercase());
    let mut count = vec![0; 26];
    for c in mes.bytes() {
        count[c as usize - 65] += 1;
    }
    count
}

/// Returns a vector of letter frequencies (faster than ngrams)
pub fn monograms(mes: &str) -> Vec<f64> {
    let count = count_letters(mes);
    let length = clean(&mes.to_ascii_uppercase()).len() as f64;
    let mut monos = vec![0.; 26];
    for index in 0..26 {
        monos[index] = count[index] as f64 / length * 100.0;
    }
    monos
}

/// Output human-readable letter frequencies
pub fn frequency(mes: &str) {
    for (index, freq) in monograms(mes).iter().enumerate() {
        println!("{}: {}%", ASCII_LOWER[index], freq);
    }
}

/// Returns the index of coincidence of a message
pub fn ioc(mes: &str) -> f64 {
    let counts = count_letters(mes);
    let score: f64 = counts
        .iter()
        .map(|c| ((*c as f64) * (*c as f64 - 1.0)))
        .sum();
    let length = clean(&mes.to_ascii_uppercase()).len() as f64;
    score / (length * (length - 1.0))
}

/// Return the chi squared score for the message
pub fn chi_squared(mes: &str) -> f64 {
    if mes.is_empty() {
        return 0.0;
    }
    let mut score = 0.0;
    let length = clean(&mes.to_ascii_uppercase()).len() as f64;
    let counts = count_letters(mes);
    for index in 0..26 {
        let expec = LETTER_FREQS[index] / 100.0 * length;
        score += (counts[index] as f64 - expec).powf(2.0) / expec;
    }
    score
}

/// Returns the monoalphabetic fitness of a given text
pub fn mono_fitness(mes: &str) -> f64 {
    let counts = count_letters(mes);
    let mut score = 0.0;
    for (index, count) in counts.iter().enumerate() {
        score += *count as f64 * LETTER_FREQS[index];
    }
    let length = clean(&mes.to_ascii_uppercase()).len() as f64;
    score / length
}

/// Returns the monoalphabetic score of a given text more efficiently
pub fn mono_score(mes: &str) -> f64 {
    let counts = count_letters(mes);
    let mut score = 0.0;
    for (index, count) in counts.iter().enumerate() {
        score += *count as f64 * LETTER_FREQS[index];
    }
    score
}

/// Checks if a ciphertext is polybius
pub fn is_polybius(mes: &str, block_length: usize) -> bool {
    let mes = mes.replace(' ', "").replace('\n', "");
    let chars = mes.chars().collect::<Vec<_>>();
    let mut pairings = chars.chunks(block_length).collect::<Vec<_>>();
    pairings.sort();
    pairings.dedup();
    chars.len() % block_length == 0 && pairings.len() >= 15 && pairings.len() <= 26
}

fn check_morse_letters(mes: &str, space_char: Option<char>) -> bool {
    let mes = if let Some(c) = space_char {
        mes.replace(c, "")
    } else {
        mes.to_string()
    };
    for word in mes.split(' ') {
        if !MORSE_LETTERS.contains(&word) {
            return false;
        }
    }
    true
}

pub enum MorseResult {
    None,
    Correct {
        dot: char,
        dash: char,
        space: Option<char>,
    },
    Unknown {
        space: Option<char>,
    },
}

/// Checks if a ciphertext is morse, if it can't find a correct answer it says it is unknown (semi-morse)
pub fn is_morse(mes: &str) -> MorseResult {
    let mut mes_chars = mes.replace(' ', "").chars().collect::<Vec<_>>();
    mes_chars.sort_unstable();
    mes_chars.dedup();
    if mes_chars.len() != 2 && mes_chars.len() != 3 {
        MorseResult::None
    } else {
        let mut space_char = None;
        for c in &mes_chars {
            if mes.matches(|l| &l == c).count() == mes.matches(&format!(" {} ", c)).count() {
                space_char = Some(*c);
            }
        }
        if let Some(c) = space_char {
            mes_chars.remove(mes_chars.iter().position(|&l| l == c).unwrap());
        }
        if mes_chars.len() != 2 || mes.split(' ').map(|m| m.len()).max().unwrap() > 5 {
            MorseResult::None
        } else {
            let test1 = mes.replace(mes_chars[0], ".").replace(mes_chars[1], "-");
            let test2 = mes.replace(mes_chars[0], "-").replace(mes_chars[1], ".");
            if check_morse_letters(&test1, space_char) {
                MorseResult::Correct {
                    dot: mes_chars[0],
                    dash: mes_chars[1],
                    space: space_char,
                }
            } else if check_morse_letters(&test2, space_char) {
                MorseResult::Correct {
                    dot: mes_chars[1],
                    dash: mes_chars[0],
                    space: space_char,
                }
            } else {
                MorseResult::Unknown { space: space_char }
            }
        }
    }
}

pub fn mono_check_reversed(mes: &str) -> bool {
    let the = Pattern::new("ThE");
    let eht = Pattern::new("EhT");
    let mut first = ' ';
    let mut first_count = 0;
    let mut second = ' ';
    let mut second_count = 0;
    for (i, count) in count_letters(mes).iter().enumerate() {
        if count > &first_count {
            second = first;
            first = ASCII_LOWER[i];
            second_count = first_count;
            first_count = *count;
        } else if count > &second_count {
            second = ASCII_LOWER[i];
            second_count = *count;
        }
    }
    let mes = mes
        .to_ascii_lowercase()
        .replace(first, "E")
        .replace(second, "T");
    the.find_best(&mes).unwrap().1 < eht.find_best(&mes).unwrap().1
}

#[derive(Clone, Debug)]
pub struct Ngrams {
    n: usize,
    dict: HashMap<String, i32>,
}

impl Ngrams {
    /// Generates an Ngrams struct for the value 'n' specified (1-4)
    pub fn new(n: usize) -> Self {
        let filename = match n {
            1 => "n-grams/monograms.txt",
            2 => "n-grams/bigrams.txt",
            3 => "n-grams/trigrams.txt",
            4 => "n-grams/quadgrams.txt",
            _ => panic!("Cannot generate {}-gram", n),
        };
        let file = File::open(filename);
        let file = match file {
            Ok(file) => file,
            Err(_) => panic!("Cannot open file!"),
        };
        let reader = BufReader::new(file);
        let mut fitnesses = HashMap::new();

        for line in reader.lines() {
            let line = match line {
                Ok(line) => line,
                Err(_) => panic!("Cannot read line!"),
            };
            let mut iter = line.split(' ');
            let word = match iter.next() {
                Some(word) => String::from(word),
                None => panic!("Cannot read word from iterator!"),
            };
            let score = match iter.next() {
                Some(score) => match score.trim().parse() {
                    Ok(num) => num,
                    Err(_) => {
                        println!("Non-integer in score!");
                        continue;
                    }
                },
                None => panic!("Cannot read score from iterator!"),
            };

            fitnesses.insert(word.clone(), score);
        }
        Self { n, dict: fitnesses }
    }

    /// Calculates the fitness of a piece of text
    pub fn fitness(&self, text: &str) -> i64 {
        let text = clean(&text.to_ascii_uppercase());
        let text_len = text.len();
        if text_len == 0 {
            return 0;
        }
        let mut fitness = 0;
        for i in 0..(text_len + 1 - self.n) {
            fitness += self
                .dict
                .get(&text[i..(i + self.n)])
                .map_or(0, |n| *n as i64);
        }
        fitness / text_len as i64
    }

    /// Does less checking than fitness so must take uppercase clean text and doesn't divide the result
    pub fn score(&self, text: &str) -> i64 {
        let text_len = text.len();
        if text_len == 0 {
            return 0;
        }
        let mut fitness = 0;
        for i in 0..(text_len + 1 - self.n) {
            let letter_fitness = self.dict.get(&text[i..(i + self.n)]);
            fitness += match letter_fitness {
                Some(n) => *n as i64,
                None => 0,
            };
        }
        fitness
    }
}

#[cfg(test)]
mod tests {
    const TEST_TEXT: &str = "Hello how are you, testing, testing, 1 2 3";

    #[test]
    fn count_letters() {
        assert_eq!(
            super::count_letters(TEST_TEXT),
            [1, 0, 0, 0, 4, 0, 2, 2, 2, 0, 0, 2, 0, 2, 3, 0, 0, 1, 2, 4, 1, 0, 1, 0, 1, 0],
        );
    }

    #[test]
    fn monograms() {
        assert_eq!(
            super::monograms(TEST_TEXT),
            [
                3.571428571428571,
                0.0,
                0.0,
                0.0,
                14.285714285714285,
                0.0,
                7.142857142857142,
                7.142857142857142,
                7.142857142857142,
                0.0,
                0.0,
                7.142857142857142,
                0.0,
                7.142857142857142,
                10.714285714285714,
                0.0,
                0.0,
                3.571428571428571,
                7.142857142857142,
                14.285714285714285,
                3.571428571428571,
                0.0,
                3.571428571428571,
                0.0,
                3.571428571428571,
                0.0
            ]
        );
    }

    #[test]
    fn ioc() {
        assert_eq!(super::ioc(TEST_TEXT), 0.05555555555555555);
    }

    #[test]
    fn chi_squared() {
        assert_eq!(super::chi_squared(TEST_TEXT), 12.935440982614303);
    }

    #[test]
    fn mono_fitness() {
        assert_eq!(super::mono_fitness(TEST_TEXT), 6.880714285714286);
    }

    #[test]
    fn ngrams() {
        let monograms = super::Ngrams::new(1);
        assert_eq!(monograms.fitness(TEST_TEXT), 300879270, "Monogram Fitness");
        assert_eq!(
            monograms.score("HELLOHOWAREYOUTESTINGTESTING"),
            8424619572,
            "Monogram Score",
        );
        let bigrams = super::Ngrams::new(2);
        assert_eq!(bigrams.fitness(TEST_TEXT), 37034576, "Bigram Fitness");
        assert_eq!(
            bigrams.score("HELLOHOWAREYOUTESTINGTESTING"),
            1036968144,
            "Bigram Score",
        );
        let trigrams = super::Ngrams::new(3);
        assert_eq!(trigrams.fitness(TEST_TEXT), 5876073, "Trigram Fitness");
        assert_eq!(
            trigrams.score("HELLOHOWAREYOUTESTINGTESTING"),
            164530071,
            "Trigram Score",
        );
        let quadgrams = super::Ngrams::new(4);
        assert_eq!(quadgrams.fitness(TEST_TEXT), 943779, "Quadgram Fitness");
        assert_eq!(
            quadgrams.score("HELLOHOWAREYOUTESTINGTESTING"),
            26425831,
            "Quadgram Score",
        );
    }
}
