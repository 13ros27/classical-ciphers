use crate::csl::{Ast, Lexer, Parser, ScopeItem, Token};
use crate::{analyse, cipher, modify, prelude::*};

use std::collections::HashMap;

#[derive(Default)]
pub struct GlobalScope {
    scope: HashMap<String, ScopeItem>,
}

pub struct Interpreter<'a> {
    tree: Ast,
    global_scope: &'a mut GlobalScope,
}

impl<'a> Interpreter<'a> {
    pub fn new(code: String, global_scope: &'a mut GlobalScope) -> Interpreter {
        let lexer = Lexer::new(&code);
        let mut parser = Parser::new(lexer);
        let tree = parser.statement();
        Interpreter { tree, global_scope }
    }

    pub fn interpret(&mut self) -> String {
        let tree = &self.tree.clone();
        format!("{}", self.visit(tree))
    }

    fn visit(&mut self, node: &Ast) -> ScopeItem {
        if node.node_type == *"Num" {
            self.visit_num(node)
        } else if node.node_type == *"String" {
            self.visit_string(node)
        } else if node.node_type == *"Func" {
            self.visit_func(node)
        } else if node.node_type == *"Assign" {
            self.visit_assign(node)
        } else if node.node_type == *"Var" {
            self.visit_var(node)
        } else if node.node_type == *"Statement" {
            self.visit_statement(node)
        } else if node.node_type == *"Expression" {
            self.visit_expression(node)
        } else {
            panic!("Not a recognised node type");
        }
    }

    fn visit_num(&self, node: &Ast) -> ScopeItem {
        if let Token::Number(value) = node.token {
            ScopeItem::F64(value)
        } else {
            panic!("Not a Num!");
        }
    }

    fn visit_string(&self, node: &Ast) -> ScopeItem {
        if let Token::Str(value) = &node.token {
            ScopeItem::Str(value.to_string())
        } else {
            panic!("Not a String!");
        }
    }

    fn visit_func(&mut self, node: &Ast) -> ScopeItem {
        let args = &node.children;
        if let Token::Func(name) = &node.token {
            match name.as_str() {
                "analyse::count_letters" => {
                    if let ScopeItem::Str(val) = self.visit(&args[0]) {
                        return ScopeItem::vec_usize(analyse::count_letters(&val));
                    }
                }
                "analyse::monograms" => {
                    if let ScopeItem::Str(val) = self.visit(&args[0]) {
                        return ScopeItem::vec_f64(analyse::monograms(&val));
                    }
                }
                "analyse::frequency" => {
                    if let ScopeItem::Str(val) = self.visit(&args[0]) {
                        analyse::frequency(&val);
                        return ScopeItem::None;
                    }
                }
                "analyse::ioc" => {
                    if let ScopeItem::Str(val) = self.visit(&args[0]) {
                        return ScopeItem::F64(analyse::ioc(&val));
                    }
                }
                "analyse::chi_squared" => {
                    if let ScopeItem::Str(val) = self.visit(&args[0]) {
                        return ScopeItem::F64(analyse::chi_squared(&val));
                    }
                }
                "analyse::mono_fitness" => {
                    if let ScopeItem::Str(val) = self.visit(&args[0]) {
                        return ScopeItem::F64(analyse::mono_fitness(&val));
                    }
                }
                "caesar::crack" => {
                    if let ScopeItem::Str(val) = self.visit(&args[0]) {
                        return ScopeItem::Str(cipher::Caesar.crack(&val).unwrap());
                    }
                }
                "affine::crack" => {
                    if let ScopeItem::Str(val) = self.visit(&args[0]) {
                        return ScopeItem::Str(cipher::Affine.crack(&val).unwrap());
                    }
                }
                "beaufort::crack" => {
                    if let ScopeItem::Str(val) = self.visit(&args[0]) {
                        if args.len() == 1 {
                            return ScopeItem::Str(cipher::Beaufort.crack(&val).unwrap());
                        } else {
                            return match self.visit(&args[1]) {
                                ScopeItem::F64(kl) => ScopeItem::Str(
                                    cipher::Beaufort.kl_crack(&val, kl as usize).unwrap(),
                                ),
                                ScopeItem::None => {
                                    ScopeItem::Str(cipher::Beaufort.crack(&val).unwrap())
                                }
                                _ => panic!("Incorrect arguments"),
                            };
                        }
                    }
                }
                "vigenere::crack" => {
                    if let ScopeItem::Str(val) = self.visit(&args[0]) {
                        if args.len() == 1 {
                            return ScopeItem::Str(cipher::Vigenere.crack(&val).unwrap());
                        } else {
                            return match self.visit(&args[1]) {
                                ScopeItem::F64(kl) => ScopeItem::Str(
                                    cipher::Vigenere.kl_crack(&val, kl as usize).unwrap(),
                                ),
                                ScopeItem::None => {
                                    ScopeItem::Str(cipher::Vigenere.crack(&val).unwrap())
                                }
                                _ => panic!("Incorrect arguments"),
                            };
                        }
                    }
                }
                "caesar::decrypt" => {
                    if let ScopeItem::Str(val) = self.visit(&args[0]) {
                        if let ScopeItem::F64(key) = self.visit(&args[1]) {
                            return ScopeItem::Str(
                                cipher::Caesar.decrypt(&val, &(key as usize)).unwrap(),
                            );
                        }
                    }
                }
                "affine::decrypt" => {
                    if let ScopeItem::Str(val) = self.visit(&args[0]) {
                        if let ScopeItem::F64(a) = self.visit(&args[1]) {
                            if let ScopeItem::F64(b) = self.visit(&args[2]) {
                                if let Some(string) =
                                    cipher::Affine.decrypt(&val, &(a as usize, b as usize))
                                {
                                    return ScopeItem::Str(string);
                                } else {
                                    return ScopeItem::None;
                                }
                            }
                        }
                    }
                }
                "vigenere::decrypt" => {
                    if let ScopeItem::Str(val) = self.visit(&args[0]) {
                        if let ScopeItem::Str(key) = self.visit(&args[1]) {
                            return ScopeItem::Str(cipher::Vigenere.decrypt(&val, &key).unwrap());
                        }
                    }
                }
                "beaufort::decrypt" => {
                    if let ScopeItem::Str(val) = self.visit(&args[0]) {
                        if let ScopeItem::Str(key) = self.visit(&args[1]) {
                            return ScopeItem::Str(cipher::Beaufort.decrypt(&val, &key).unwrap());
                        }
                    }
                }
                "caesar::encrypt" => {
                    if let ScopeItem::Str(val) = self.visit(&args[0]) {
                        if let ScopeItem::F64(key) = self.visit(&args[1]) {
                            return ScopeItem::Str(
                                cipher::Caesar.encrypt(&val, &(key as usize)).unwrap(),
                            );
                        }
                    }
                }
                "affine::encrypt" => {
                    if let ScopeItem::Str(val) = self.visit(&args[0]) {
                        if let ScopeItem::F64(a) = self.visit(&args[1]) {
                            if let ScopeItem::F64(b) = self.visit(&args[2]) {
                                return ScopeItem::Str(
                                    cipher::Affine
                                        .encrypt(&val, &(a as usize, b as usize))
                                        .unwrap(),
                                );
                            }
                        }
                    }
                }
                "vigenere::encrypt" => {
                    if let ScopeItem::Str(val) = self.visit(&args[0]) {
                        if let ScopeItem::Str(key) = self.visit(&args[1]) {
                            return ScopeItem::Str(cipher::Vigenere.encrypt(&val, &key).unwrap());
                        }
                    }
                }
                "beaufort::encrypt" => {
                    if let ScopeItem::Str(val) = self.visit(&args[0]) {
                        if let ScopeItem::Str(key) = self.visit(&args[1]) {
                            return ScopeItem::Str(cipher::Beaufort.encrypt(&val, &key).unwrap());
                        }
                    }
                }
                "modify::split" => {
                    if let ScopeItem::Str(val) = self.visit(&args[0]) {
                        if let ScopeItem::F64(parts) = self.visit(&args[1]) {
                            return ScopeItem::vec_str(modify::split(&val, parts as usize));
                        }
                    }
                }
                "modify::rejoin" => {
                    if let ScopeItem::Vector(scope_texts) = self.visit(&args[0]) {
                        let mut texts = Vec::new();
                        for text in scope_texts {
                            if let ScopeItem::Str(t) = text {
                                texts.push(t);
                            }
                        }
                        return ScopeItem::Str(modify::rejoin(texts));
                    }
                }
                _ => panic!("Unrecognised function"),
            }
            panic!("Incorrect arguments");
        } else {
            panic!("Not a func!");
        }
    }

    fn visit_assign(&mut self, node: &Ast) -> ScopeItem {
        if let Token::Id(key) = &node.token {
            let value = self.visit(&node.children[0]);
            self.global_scope
                .scope
                .insert(key.to_string(), value.clone());
            value
        } else {
            panic!("Not assigning to an Id");
        }
    }

    fn visit_var(&mut self, node: &Ast) -> ScopeItem {
        if let Token::Id(key) = node.token.clone() {
            let value = self.global_scope.scope.get::<String>(&key);
            if let Some(item) = value {
                item.clone()
            } else {
                panic!("No variable with the name '{}'", key);
            }
        } else {
            panic!("Not a Var");
        }
    }

    fn visit_statement(&mut self, node: &Ast) -> ScopeItem {
        self.visit(&node.children[0]);
        ScopeItem::None
    }

    fn visit_expression(&mut self, node: &Ast) -> ScopeItem {
        self.visit(&node.children[0])
    }
}
