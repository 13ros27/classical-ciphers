#[derive(Clone, Debug)]
pub(crate) enum ScopeItem {
    Str(String),
    Vector(Vec<ScopeItem>),
    F64(f64),
    // Table(Vec<Vec<ScopeItem>>),
    None,
}

impl ScopeItem {
    pub(crate) fn vec_usize(vector: Vec<usize>) -> ScopeItem {
        let mut ret = Vec::new();
        for arg in &vector {
            ret.push(ScopeItem::F64((*arg) as f64));
        }
        ScopeItem::Vector(ret)
    }

    pub(crate) fn vec_f64(vector: Vec<f64>) -> ScopeItem {
        let mut ret = Vec::new();
        for arg in &vector {
            ret.push(ScopeItem::F64(*arg));
        }
        ScopeItem::Vector(ret)
    }

    pub(crate) fn vec_str(vector: Vec<String>) -> ScopeItem {
        let mut ret = Vec::new();
        for arg in &vector {
            ret.push(ScopeItem::Str(arg.to_string()));
        }
        ScopeItem::Vector(ret)
    }
}

impl std::fmt::Display for ScopeItem {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        match self {
            Self::Str(val) => write!(f, "{}", val),
            Self::Vector(val) => {
                let mut ret = Vec::new();
                for arg in val {
                    ret.push(format!("{}", arg));
                }
                write!(f, "[{}]", ret.join(", "))
            }
            Self::F64(val) => write!(f, "{}", val),
            // Self::Table(_val) => write!(f, "Not yet implemented display of tables"),
            Self::None => write!(f, ""),
        }
    }
}
