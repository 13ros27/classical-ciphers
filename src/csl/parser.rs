use crate::csl::{Ast, Lexer, Token};

fn variant_eq(a: &Token, b: &Token) -> bool {
    std::mem::discriminant(a) == std::mem::discriminant(b)
}

pub struct Parser<'a> {
    lexer: Lexer<'a>,
    current_token: Token,
}
impl<'a> Parser<'a> {
    pub fn new(mut lexer: Lexer<'a>) -> Parser {
        let current_token = lexer.get_next_token(true);
        Parser {
            lexer,
            current_token,
        }
    }

    fn eat(&mut self, token_type: Token) {
        if variant_eq(&self.current_token, &token_type) {
            self.current_token = self.lexer.get_next_token(true);
        } else {
            panic!("Syntax Error: {}", self.lexer.pos as i64 - 1);
        }
    }

    /// component: function | String | Number | Id
    fn component(&mut self) -> Ast {
        let token = self.current_token.clone();
        match token {
            Token::Str(_) => {
                self.eat(Token::Str(String::new()));
                Ast::string(token, self.lexer.pos - 1)
            }
            Token::Number(_) => {
                self.eat(Token::Number(0.0));
                Ast::num(token, self.lexer.pos - 1)
            }
            Token::Func(_) => self.function(),
            Token::Id(_) => {
                self.eat(Token::Id(String::new()));
                Ast::var(token, self.lexer.pos - 1)
            }
            _ => unreachable!(),
        }
    }

    /// function: Func Lparen function_arguments Rparen
    fn function(&mut self) -> Ast {
        let pos = self.lexer.pos;
        let func = self.current_token.clone();
        self.eat(Token::Func(String::new()));
        self.eat(Token::Lparen);
        let args = self.function_arguments();
        self.eat(Token::Rparen);
        Ast::func(func, args, pos)
    }

    /// function_arguments: ?(component (Comma component)*)
    fn function_arguments(&mut self) -> Vec<Ast> {
        if let Token::Rparen = self.current_token.clone() {
            return Vec::new();
        }
        let mut args = vec![self.component()];
        loop {
            if let Token::Comma = self.current_token.clone() {
                self.eat(Token::Comma);
                args.push(self.component());
            } else {
                return args;
            }
        }
    }

    /// assignment: Let Id Assign component
    fn assignment(&mut self) -> Ast {
        self.eat(Token::Let);
        let token = self.current_token.clone();
        let pos = self.lexer.pos;
        self.eat(Token::Id(String::new()));
        self.eat(Token::Assign);
        let comp = self.component();
        Ast::assign(token, comp, pos)
    }

    /// statement: (assignment | component) ?Semicolon
    pub(crate) fn statement(&mut self) -> Ast {
        let token = self.current_token.clone();
        let value = match token {
            Token::Let => self.assignment(),
            _ => self.component(),
        };
        if let Token::Semicolon = self.current_token.clone() {
            self.eat(Token::Semicolon);
            Ast::statement(token, value, self.lexer.pos - 1)
        } else {
            Ast::expression(token, value, self.lexer.pos - 1)
        }
    }
}
