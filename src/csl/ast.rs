use crate::csl::Token;

#[derive(Debug, Clone)]
pub struct Ast {
    pub node_type: String,
    pub token: Token,
    pub children: Vec<Ast>,
    pub loc: usize,
}
impl Ast {
    pub fn num(token: Token, loc: usize) -> Ast {
        Ast {
            node_type: String::from("Num"),
            token,
            children: Vec::new(),
            loc,
        }
    }

    pub fn string(token: Token, loc: usize) -> Ast {
        Ast {
            node_type: String::from("String"),
            token,
            children: Vec::new(),
            loc,
        }
    }

    pub fn func(op: Token, args: Vec<Ast>, loc: usize) -> Ast {
        Ast {
            node_type: String::from("Func"),
            token: op,
            children: args,
            loc,
        }
    }

    pub fn assign(op: Token, value: Ast, loc: usize) -> Ast {
        Ast {
            node_type: String::from("Assign"),
            token: op,
            children: vec![value],
            loc,
        }
    }

    pub fn var(op: Token, loc: usize) -> Ast {
        Ast {
            node_type: String::from("Var"),
            token: op,
            children: Vec::new(),
            loc,
        }
    }

    pub fn statement(op: Token, value: Ast, loc: usize) -> Ast {
        Ast {
            node_type: String::from("Statement"),
            token: op,
            children: vec![value],
            loc,
        }
    }

    pub fn expression(op: Token, value: Ast, loc: usize) -> Ast {
        Ast {
            node_type: String::from("Expression"),
            token: op,
            children: vec![value],
            loc,
        }
    }
}
