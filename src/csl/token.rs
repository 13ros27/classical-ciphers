#[derive(Clone, Debug)]
pub enum Token {
    Str(String),
    Number(f64),
    Func(String),
    Id(String),
    Semicolon,
    Let,
    Assign,
    Lparen,
    Rparen,
    Comma,
    Eol,
}
