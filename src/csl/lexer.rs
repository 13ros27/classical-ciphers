use crate::csl::Token;

#[allow(dead_code)]
pub fn debug_lexer(mut lexer: Lexer) {
    loop {
        let token = lexer.get_next_token(true);
        if let Token::Eol = token {
            break;
        }
        println!("{:?}", token);
    }
}

pub struct Lexer<'a> {
    text: &'a str,
    pub pos: usize,
    current_char: char,
    func_names: Vec<String>,
}

impl<'a> Lexer<'a> {
    pub fn new(text: &'a str) -> Lexer {
        Lexer {
            text,
            pos: 0,
            current_char: text.as_bytes()[0] as char,
            func_names: vec![
                "analyse::count_letters".to_string(),
                "analyse::monograms".to_string(),
                "analyse::frequency".to_string(),
                "analyse::ioc".to_string(),
                "analyse::chi_squared".to_string(),
                "analyse::mono_fitness".to_string(),
                "crack::caesar".to_string(),
                "crack::affine".to_string(),
                "crack::bellaso".to_string(),
                "crack::vigenere".to_string(),
                "decrypt::caesar".to_string(),
                "decrypt::affine".to_string(),
                "decrypt::vigenere".to_string(),
                "decrypt::bellaso".to_string(),
                "encrypt::caesar".to_string(),
                "encrypt::affine".to_string(),
                "encrypt::vigenere".to_string(),
                "encrypt::bellaso".to_string(),
                "modify::split".to_string(),
                "modify::rejoin".to_string(),
            ],
        }
    }

    fn advance(&mut self) {
        self.pos += 1;
        if self.pos > self.text.len() - 1 {
            self.current_char = 0 as char;
        } else {
            self.current_char = self.text.as_bytes()[self.pos] as char;
        }
    }

    fn skip_whitespace(&mut self) {
        while self.current_char != 0 as char
            && (self.current_char == ' ' || self.current_char == '\t')
        {
            self.advance();
        }
    }

    fn _id(&mut self) -> Token {
        let mut result = String::new();
        while self.current_char.is_alphanumeric()
            || self.current_char == '_'
            || self.current_char == ':'
        {
            result.push(self.current_char);
            self.advance();
        }
        if result == "let" {
            Token::Let
        } else if self.func_names.contains(&result) {
            Token::Func(result)
        } else {
            Token::Id(result)
        }
    }

    fn number(&mut self) -> f64 {
        let mut result: f64 = 0.0;
        let mut decimal = 0;
        while self.current_char != 0 as char
            && (self.current_char.is_digit(10) || self.current_char == '.')
        {
            if self.current_char == '.' {
                if decimal != 0 {
                    panic!("Syntax Error: {}", self.pos as i64);
                }
                decimal = 1;
                self.advance();
                continue;
            }
            if decimal == 0 {
                result *= 10.0;
                result += (self.current_char as i32) as f64 - 48.0;
            } else {
                result += ((self.current_char as i32) as f64 - 48.0) / 10_f64.powf(decimal.into());
                decimal += 1
            }
            self.advance();
        }
        result as f64
    }

    fn string(&mut self) -> String {
        let mut result = String::new();
        self.advance();
        while self.current_char != 0 as char && self.current_char != '"' {
            result.push(self.current_char);
            self.advance();
        }
        result
    }

    pub fn get_next_token(&mut self, advance: bool) -> Token {
        if self.current_char == 0 as char {
            return Token::Eol;
        }

        if self.current_char == ' ' || self.current_char == '\t' {
            self.skip_whitespace();
            return self.get_next_token(advance);
        } else if self.current_char.is_digit(10) || self.current_char == '.' {
            return Token::Number(self.number());
        } else if self.current_char.is_alphabetic() || self.current_char == '_' {
            return self._id();
        }

        let token = match self.current_char {
            '=' => Token::Assign,
            ';' => Token::Semicolon,
            '(' => Token::Lparen,
            ')' => Token::Rparen,
            ',' => Token::Comma,
            '"' => Token::Str(self.string()),
            _ => panic!("Syntax Error: {}", self.pos as i64),
        };
        if advance {
            self.advance();
        }
        token
    }
}
