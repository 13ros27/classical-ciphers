use std::io::{self, Write};

mod ast;
mod interpreter;
mod lexer;
mod parser;
mod scope_item;
mod token;

pub(crate) use ast::Ast;
pub(crate) use interpreter::{GlobalScope, Interpreter};
pub(crate) use lexer::Lexer;
pub(crate) use parser::Parser;
pub(crate) use scope_item::ScopeItem;
pub(crate) use token::Token;

pub fn ide() {
    let mut scope = GlobalScope::default();
    loop {
        print!(">>> ");
        io::stdout().flush().unwrap();
        let mut line = String::new();
        io::stdin()
            .read_line(&mut line)
            .expect("Failed to read line");
        let ret = Interpreter::new(line.trim().to_string(), &mut scope).interpret();
        if !ret.is_empty() {
            println!("{}", ret);
        }
    }
}
