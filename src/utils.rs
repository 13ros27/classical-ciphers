#![allow(dead_code)]
use std::cmp::PartialOrd;

pub struct BestScore<A: Default, T: Copy + PartialOrd> {
    score: Option<T>,
    pub arg: A,
    pub last_changed: usize,
}
impl<A: Default, T: Copy + PartialOrd> BestScore<A, T> {
    pub fn new() -> Self {
        let arg = A::default();
        Self {
            score: None,
            arg,
            last_changed: 0,
        }
    }

    pub fn add(&mut self, score: T, arg: A) {
        if let Some(old_score) = self.score {
            if score <= old_score {
                self.last_changed += 1;
                return;
            }
        }
        self.score = Some(score);
        self.arg = arg;
        self.last_changed = 0;
    }

    /// Makes score semi-private
    pub fn score(&self) -> Option<T> {
        self.score
    }
}
impl<A: Default, T: Copy + PartialOrd> Default for BestScore<A, T> {
    fn default() -> Self {
        Self::new()
    }
}

pub(crate) fn generate_keyword_alphabet(keyword: &[usize]) -> Vec<usize> {
    let mut alphabet = keyword.to_owned();
    for index in 0..26 {
        if keyword.contains(&index) {
            continue;
        }
        alphabet.push(index);
    }
    alphabet
}

pub(crate) fn generate_alt_keyword_alphabet(keyword: &[usize]) -> Vec<usize> {
    let mut alphabet = keyword.to_owned();
    for index in 0..26 {
        if keyword.contains(&((keyword.last().unwrap() + index) % 26)) {
            continue;
        }
        alphabet.push((keyword.last().unwrap() + index) % 26);
    }
    alphabet
}

#[cfg(test)]
mod tests {
    #[test]
    fn best_score() {
        let mut best = super::BestScore::new();
        best.add(5.0, "item1".to_string());
        assert_eq!(best.arg, "item1".to_string());
        best.add(3.7, "item2".to_string());
        assert_eq!(best.arg, "item1".to_string());
        best.add(7.0, "item3".to_string());
        assert_eq!(best.arg, "item3".to_string());
    }
}
