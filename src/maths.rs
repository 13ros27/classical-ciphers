#![allow(dead_code)]
use phf::phf_map;

pub static MMI_26: phf::Map<u8, usize> = phf_map! {
    1u8 => 1, 3u8 => 9, 5u8 => 21, 7u8 => 15, 9u8 => 3, 11u8 => 19,
    15u8 => 7, 17u8 => 23, 19u8 => 11, 21u8 => 5, 23u8 => 17, 25u8 => 25,
};

/// Determinant of a 2x2 matrix stored as a vector
pub fn determinant2(mat: &Vec<isize>) -> isize {
    (mat[0] * mat[3]) - (mat[1] * mat[2])
}

/// Inverse of a 2x2 matrix
pub fn inverse2(mat: &Vec<isize>, det: isize) -> Vec<isize> {
    vec![
        (mat[3] % 26) * (MMI_26[&(det as u8)] as isize) % 26,
        ((-mat[1] % 26) * MMI_26[&(det as u8)] as isize) % 26,
        ((-mat[2] % 26) * MMI_26[&(det as u8)] as isize) % 26,
        ((mat[0] % 26) * MMI_26[&(det as u8)] as isize) % 26,
    ]
}
