#![allow(dead_code)]
use std::{
    default::Default,
    fmt,
    fmt::{Display, Formatter},
};

use crate::{analyse, cipher, keylength, modify, prelude::*};

#[derive(Clone, Debug)]
pub enum Cipher {
    Caesar(Modifier),
    Affine(Modifier),
    Vigenere(Modifier),
    Beaufort(Modifier),
    PeriodicAffine(Modifier),
    Permutation(Modifier),
    ColumnarTransposition(Modifier),
    UnknownTransposition(Modifier),
    Railfence(Modifier),
    MonoSubstitution(Modifier),
    Hill2x2(Modifier),
    PolySubstitution(Vec<usize>),
}

impl Display for Cipher {
    fn fmt(&self, f: &mut Formatter<'_>) -> Result<(), fmt::Error> {
        match self {
            Self::Caesar(modif) => write!(f, "{}Caesar Cipher", modif),
            Self::Affine(modif) => write!(f, "{}Affine Cipher", modif),
            Self::Vigenere(modif) => write!(f, "{}Vigenere Cipher", modif),
            Self::Beaufort(modif) => write!(f, "{}Beaufort Cipher", modif),
            Self::PeriodicAffine(modif) => write!(f, "{}Periodic Affine Cipher", modif),
            Self::Permutation(modif) => write!(f, "{}Permutation Cipher", modif),
            Self::ColumnarTransposition(modif) => {
                write!(f, "{}Columnar Transposition Cipher", modif)
            }
            Self::UnknownTransposition(modif) => {
                write!(f, "{}Unknown Transposition Cipher", modif)
            }
            Self::Railfence(modif) => write!(f, "{}Railfence Cipher", modif),
            Self::MonoSubstitution(modif) => {
                write!(f, "{}Monoalphabetic Substitution Cipher", modif)
            }
            Self::Hill2x2(modif) => write!(f, "{}Hill Cipher (2x2)", modif),
            Self::PolySubstitution(kls) => {
                if kls.is_empty() {
                    write!(
                        f,
                        "Polyalphabetic Substitution Cipher with unknown keylength"
                    )
                } else if kls.len() == 1 {
                    write!(
                        f,
                        "Polyalphabetic Substitution Cipher with keylength {:?}",
                        kls[0]
                    )
                } else {
                    write!(
                        f,
                        "Polyalphabetic Substitution Cipher with possible keylengths {:?}",
                        kls
                    )
                }
            }
        }
    }
}

#[derive(Clone, Debug)]
pub enum Modifier {
    None,
    Reverse(Option<Box<Modifier>>),
    Unknown(Option<Box<Modifier>>),
    Polybius(Option<Box<Modifier>>),
    SemiMorse(Option<Box<Modifier>>),
    Morse(Option<Box<Modifier>>),
    Double(Box<Cipher>),
}

impl Display for Modifier {
    fn fmt(&self, f: &mut Formatter<'_>) -> Result<(), fmt::Error> {
        let (payload, modif) = match self {
            Self::None => {
                return write!(f, "");
            }
            Self::Reverse(modif) => ("Reversed ".to_string(), modif),
            Self::Unknown(modif) => (String::new(), modif),
            Self::Polybius(modif) => ("Polybius ".to_string(), modif),
            Self::SemiMorse(modif) => ("Semi-Morse ".to_string(), modif),
            Self::Morse(modif) => ("Morse ".to_string(), modif),
            Self::Double(cipher) => {
                let cs = format!("{}", cipher);
                (cs[0..cs.len() - 6].to_string(), &None)
            }
        };
        if let Some(inner_mod) = modif {
            write!(f, "{}{}", &payload, inner_mod)
        } else {
            write!(f, "{}", &payload)
        }
    }
}

impl Modifier {
    fn enclose(self, inner: Modifier) -> Modifier {
        if let Self::None = inner {
            self
        } else {
            match self {
                Self::None => inner,
                Self::Reverse(_) => Self::Reverse(Some(Box::new(inner))),
                Self::Unknown(_) => Self::Unknown(Some(Box::new(inner))),
                Self::Polybius(_) => Self::Polybius(Some(Box::new(inner))),
                Self::SemiMorse(_) => Self::SemiMorse(Some(Box::new(inner))),
                Self::Morse(_) => Self::Morse(Some(Box::new(inner))),
                Self::Double(_) => unreachable!(),
            }
        }
    }
}

#[derive(Clone, Copy, Debug)]
pub struct Parameters {
    pub min_fitness: f64,
    pub ioc_range: (f64, f64),
    pub bigram_fitness: i64,
}

impl Default for Parameters {
    fn default() -> Self {
        Parameters {
            min_fitness: 6.0,
            ioc_range: (0.057, 0.075),
            bigram_fitness: 27000000,
        }
    }
}

fn test_modified<C>(
    ciphertext: &str,
    cipher: C,
    bigrams: &analyse::Ngrams,
    modifier: &Modifier,
    params: &Parameters,
) -> Option<(String, Modifier)>
where
    C: cipher::Cipher + Crack,
{
    let pos = cipher.crack(ciphertext).unwrap();
    let fitness = bigrams.fitness(&pos);
    let reverse_pos: String = pos.chars().rev().collect();
    let rev_fitness = bigrams.fitness(&reverse_pos);
    let (outer_mod, pl) = if fitness > params.bigram_fitness && fitness > rev_fitness {
        (Modifier::None, pos)
    } else if rev_fitness > params.bigram_fitness {
        (Modifier::Reverse(None), reverse_pos)
    } else if analyse::mono_fitness(&pos) > params.min_fitness {
        let (plaintext, cip) = transposition(&pos, bigrams, modifier, params, false);
        (Modifier::Double(Box::new(cip)), plaintext)
    } else {
        return None;
    };
    Some((pl, outer_mod.enclose(modifier.clone())))
}

fn test_true_reversed<C>(
    ciphertext: &str,
    cipher: C,
    bigrams: &analyse::Ngrams,
    modifier: &Modifier,
    params: &Parameters,
) -> Option<(String, Modifier)>
where
    C: cipher::Cipher + Crack,
{
    let pos = cipher.crack(ciphertext).unwrap();
    let fitness = bigrams.fitness(&pos);
    let reverse_pos: String = cipher
        .crack(&ciphertext.chars().rev().collect::<String>())
        .unwrap();
    let rev_fitness = bigrams.fitness(&reverse_pos);
    let (outer_mod, pl) = if fitness > params.bigram_fitness && fitness > rev_fitness {
        (Modifier::None, pos)
    } else if rev_fitness > params.bigram_fitness {
        (Modifier::Reverse(None), reverse_pos)
    } else {
        return None;
    };
    Some((pl, outer_mod.enclose(modifier.clone())))
}

fn polybius(ciphertext: &str, modifier: &Modifier) -> (String, Modifier) {
    if let Some(decrypt) = cipher::Polybius.crack(ciphertext) {
        (decrypt, Modifier::Polybius(None).enclose(modifier.clone()))
    } else {
        (ciphertext.to_string(), modifier.clone())
    }
}

fn base_modifiers(ciphertext: &str) -> (String, Modifier) {
    let (ciphertext, modif) = polybius(ciphertext, &Modifier::None);
    let analyse_morse = analyse::is_morse(&ciphertext);
    let (ciphertext, modif) =
        if let analyse::MorseResult::Correct { dot, dash, space } = analyse_morse {
            (
                modify::morse(&ciphertext, dot, dash, space),
                Modifier::Morse(None).enclose(modif),
            )
        } else if let analyse::MorseResult::Unknown { space } = analyse_morse {
            (
                modify::semi_morse(&ciphertext, space),
                Modifier::SemiMorse(None).enclose(modif),
            )
        } else {
            (ciphertext, modif)
        };
    let (ciphertext, modif) = polybius(&ciphertext, &modif);
    (ciphertext, modif)
}

fn transposition(
    ciphertext: &str,
    bigrams: &analyse::Ngrams,
    modif: &Modifier,
    params: &Parameters,
    base_trans: bool,
) -> (String, Cipher) {
    if let Some((plaintext, modifier)) =
        test_true_reversed(ciphertext, cipher::Railfence, bigrams, modif, params)
    {
        (plaintext, Cipher::Railfence(modifier))
    } else if let Some((plaintext, modifier)) =
        test_true_reversed(ciphertext, cipher::Permutation, bigrams, modif, params)
    {
        (plaintext, Cipher::Permutation(modifier))
    } else if let Some((plaintext, modifier)) =
        test_true_reversed(ciphertext, cipher::ColTransposition, bigrams, modif, params)
    {
        (plaintext, Cipher::ColumnarTransposition(modifier))
    } else {
        if base_trans {
            let mono_cp = cipher::MonoSubstitution.crack(ciphertext).unwrap();
            if bigrams.fitness(&mono_cp) > params.bigram_fitness {
                return (mono_cp, Cipher::MonoSubstitution(modif.clone()));
            }
        }
        (
            ciphertext.to_string(),
            Cipher::UnknownTransposition(modif.clone()),
        )
    }
}

fn mono_substitution(
    ciphertext: &str,
    bigrams: &analyse::Ngrams,
    modif: &Modifier,
    params: &Parameters,
) -> (String, Cipher) {
    let clean_text = modify::clean(ciphertext);
    if let Some((plaintext, modifier)) =
        test_modified(ciphertext, cipher::Caesar, bigrams, modif, params)
    {
        (plaintext, Cipher::Caesar(modifier))
    } else if let Some((plaintext, modifier)) =
        test_modified(ciphertext, cipher::Affine, bigrams, modif, params)
    {
        (plaintext, Cipher::Affine(modifier))
    } else {
        let reversed = analyse::mono_check_reversed(&clean_text);
        let (clean_text, reversed_modifier) = if reversed {
            (
                clean_text.chars().rev().collect::<String>(),
                Modifier::Reverse(None),
            )
        } else {
            (clean_text, Modifier::None)
        };
        let modif = reversed_modifier.enclose(modif.clone());
        (
            cipher::MonoSubstitution
                .crack(&clean_text.to_ascii_lowercase())
                .unwrap(),
            Cipher::MonoSubstitution(modif),
        )
    }
}

fn poly_substitution(
    ciphertext: &str,
    bigrams: &analyse::Ngrams,
    modif: &Modifier,
    params: &Parameters,
) -> (String, Cipher) {
    let clean_text = modify::clean(ciphertext);
    if let Some((plaintext, modifier)) =
        test_modified(ciphertext, cipher::Vigenere, bigrams, modif, params)
    {
        (plaintext, Cipher::Vigenere(modifier))
    } else if let Some((plaintext, modifier)) =
        test_modified(ciphertext, cipher::Beaufort, bigrams, modif, params)
    {
        (plaintext, Cipher::Beaufort(modifier))
    } else if let Some((plaintext, modifier)) =
        test_modified(ciphertext, cipher::PeriodicAffine, bigrams, modif, params)
    {
        (plaintext, Cipher::PeriodicAffine(modifier))
    } else {
        if clean_text.len() % 2 == 0 {
            let hill_pt = cipher::Hill2x2.crack(ciphertext).unwrap();
            if bigrams.fitness(&hill_pt) > params.bigram_fitness {
                return (hill_pt, Cipher::Hill2x2(modif.clone()));
            }
        }
        let kl = keylength::polyalphabetic(&clean_text, 50, params.ioc_range);
        (
            clean_text.to_ascii_lowercase(),
            Cipher::PolySubstitution(kl),
        )
    }
}

pub fn crack(ciphertext: &str, params: &Parameters) -> (String, Cipher) {
    let bigrams = analyse::Ngrams::new(2);
    let (ciphertext, modif) = base_modifiers(ciphertext);
    let mes = modify::clean(&ciphertext);
    let mono_fitness = analyse::mono_fitness(&mes);
    let ioc = analyse::ioc(&mes);
    if params.ioc_range.1 > ioc && ioc > params.ioc_range.0 {
        if mono_fitness > params.min_fitness {
            transposition(&ciphertext, &bigrams, &modif, params, true)
        } else {
            mono_substitution(&ciphertext, &bigrams, &modif, params)
        }
    } else {
        poly_substitution(&ciphertext, &bigrams, &modif, params)
    }
}

#[macro_export]
macro_rules! crack {
    ( $ciphertext: expr$(,$i: ident = $e: expr)* ) => {
        {
            let params = crate::auto::Parameters {
                $($i: $e as f64,)*
                ..Default::default()
            };
            crate::auto::crack($ciphertext, &params)
        }
    };
}
