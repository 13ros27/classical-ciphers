use crate::{analyse, modify};

/// Returns the possible keylengths of a given polyalphabetic cipher
pub fn polyalphabetic(mes: &str, max_len: usize, ioc_range: (f64, f64)) -> Vec<usize> {
    let mes = modify::clean(mes);
    let mut kls = Vec::new();
    for pos_kl in 1..max_len {
        if kls.iter().any(|kl| pos_kl % *kl == 0) {
            continue;
        }
        if pos_kl >= mes.len() / 2 {
            break;
        }
        let index: f64 = modify::split(&mes, pos_kl)
            .into_iter()
            .map(|t| analyse::ioc(&t))
            .sum::<f64>()
            / pos_kl as f64;
        if ioc_range.0 < index && index < ioc_range.1 {
            kls.push(pos_kl);
        }
    }
    kls
}
