#![feature(min_specialization)]
#![allow(dead_code, clippy::ptr_arg)]

pub mod analyse;
pub mod auto;
pub mod cipher;
pub mod csl;
pub mod keylength;
pub mod maths;
pub mod modify;
pub mod pattern;
pub mod prelude;
pub mod utils;
