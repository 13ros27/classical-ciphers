mod affine;
mod beaufort;
mod caesar;
mod columnar_transposition;
mod hill2x2;
mod hill_climbing;
mod mono_substitution;
mod periodic_affine;
mod permutation;
mod polybius;
mod railfence;
mod utils;
mod vigenere;

use crate::{analyse::Ngrams, utils::BestScore};

pub use {
    affine::Affine, beaufort::Beaufort, caesar::Caesar, columnar_transposition::ColTransposition,
    hill2x2::Hill2x2, mono_substitution::MonoSubstitution, periodic_affine::PeriodicAffine,
    permutation::Permutation, polybius::Polybius, railfence::Railfence, vigenere::Vigenere,
};

pub trait Cipher: Copy {
    type Key;
    fn decrypt(&self, mes: &str, key: &Self::Key) -> Option<String>;
    fn encrypt(&self, mes: &str, key: &Self::Key) -> Option<String>;
}

pub trait KeySpace: Cipher {
    fn keyspace(&self) -> Vec<Self::Key>;
}

pub trait Crack: Cipher {
    fn crack(&self, mes: &str) -> Option<String>;
}

pub trait KLCrack: Cipher {
    fn kl_crack(&self, mes: &str, keylength: usize) -> Option<String>;
}

impl<T: KLCrack> Crack for T {
    default fn crack(&self, mes: &str) -> Option<String> {
        let mut best = BestScore::new();
        let bigrams = Ngrams::new(2);
        for pos_kl in 2..16 {
            let pos_dec = self.kl_crack(mes, pos_kl).unwrap();
            best.add(bigrams.fitness(&pos_dec) as f64, pos_dec);
        }
        Some(best.arg)
    }
}
