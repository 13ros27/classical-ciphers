use super::{utils::threaded_brute_force, Cipher, Crack, KeySpace};
use crate::{
    analyse::{Ngrams, ASCII_LOWER},
    maths::{determinant2, inverse2, MMI_26},
    modify::{clean_verbose, expand, string_to_num},
};
use std::fs::read_to_string;

#[derive(Clone, Copy)]
pub struct Hill2x2;

impl KeySpace for Hill2x2 {
    fn keyspace(&self) -> Vec<String> {
        read_to_string("keys/hill2x2.txt")
            .expect("Unable to read file")
            .lines()
            .map(|l| l.to_string())
            .collect()
    }
}

fn crypt(mes: &str, num_key: Vec<isize>) -> String {
    let (mes, expansion) = clean_verbose(mes);
    let num_mes = string_to_num(&mes);
    let pairs = num_mes
        .chunks(2)
        .map(|n| [n[0] as isize, n[1] as isize])
        .collect::<Vec<_>>();
    let mut result = Vec::new();
    for pair in pairs {
        let mut new_pair = String::new();
        for i in 0..2 {
            new_pair.push(
                ASCII_LOWER[((num_key[2 * i] * pair[0] + num_key[2 * i + 1] * pair[1])
                    .rem_euclid(26)) as usize],
            )
        }
        result.push(new_pair)
    }
    expand(&result.join(""), expansion)
}

impl Cipher for Hill2x2 {
    type Key = String;

    fn decrypt(&self, mes: &str, key: &String) -> Option<String> {
        let num_key = &string_to_num(key).iter().map(|n| *n as isize).collect();
        let det = determinant2(num_key).rem_euclid(26);
        MMI_26.get(&(det as u8))?;
        let adjmat = inverse2(num_key, det);
        Some(crypt(mes, adjmat))
    }

    fn encrypt(&self, mes: &str, key: &String) -> Option<String> {
        let num_key = string_to_num(key).iter().map(|n| *n as isize).collect();
        let det = determinant2(&num_key).rem_euclid(26);
        MMI_26.get(&(det as u8))?;
        Some(crypt(mes, num_key))
    }
}

impl Crack for Hill2x2 {
    fn crack(&self, mes: &str) -> Option<String> {
        let bigrams = Ngrams::new(2);
        Some(threaded_brute_force(
            mes,
            &self.keyspace(),
            &Self,
            &|t: &str| bigrams.fitness(t),
            12,
        ))
    }
}
