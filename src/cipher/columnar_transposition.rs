use super::{Cipher, Crack, KLCrack, Permutation};
use crate::{
    analyse::Ngrams,
    modify::{clean_verbose, expand, invert_transposition},
    utils::BestScore,
};

#[derive(Clone, Copy)]
pub struct ColTransposition;

impl Cipher for ColTransposition {
    type Key = Vec<usize>;

    fn decrypt(&self, _mes: &str, _key: &Vec<usize>) -> Option<String> {
        todo!();
    }

    fn encrypt(&self, _mes: &str, _key: &Vec<usize>) -> Option<String> {
        todo!();
    }
}

impl KLCrack for ColTransposition {
    fn kl_crack(&self, mes: &str, keylength: usize) -> Option<String> {
        Permutation.kl_crack(&invert_transposition(mes, keylength), keylength)
    }
}

impl Crack for ColTransposition {
    fn crack(&self, mes: &str) -> Option<String> {
        let (mes, expansion) = clean_verbose(mes);
        let mut best = BestScore::new();
        let bigrams = Ngrams::new(2);
        for pos_kl in 2..10 {
            if mes.len() % pos_kl != 0 {
                continue;
            }
            let pos_dec = self.kl_crack(&mes, pos_kl).unwrap();
            let fitness = bigrams.fitness(&pos_dec);
            best.add(fitness, pos_dec);
            if fitness > 28000000 {
                break;
            }
        }
        Some(expand(&best.arg, expansion))
    }
}
