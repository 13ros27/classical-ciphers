use super::{
    utils::{brute_force, mono_substitution},
    Cipher, Crack, KeySpace,
};
use crate::analyse::mono_score;

#[derive(Clone, Copy)]
pub struct Caesar;

impl KeySpace for Caesar {
    fn keyspace(&self) -> Vec<usize> {
        (0..26).collect()
    }
}

impl Cipher for Caesar {
    type Key = usize;

    fn decrypt(&self, mes: &str, key: &usize) -> Option<String> {
        Some(mono_substitution(mes, &|x| ((x + 26 - (key % 26)) % 26)))
    }

    fn encrypt(&self, mes: &str, key: &usize) -> Option<String> {
        Some(mono_substitution(mes, &|x| (x + key) % 26))
    }
}

impl Crack for Caesar {
    fn crack(&self, mes: &str) -> Option<String> {
        Some(brute_force(mes, &self.keyspace(), &Self, &mono_score))
    }
}

#[cfg(test)]
mod tests {
    use crate::prelude::*;
    const TEST_TEXT: &str = "Hello how are you, testing, testing, 1 2 3";

    #[test]
    fn decrypt() {
        assert_eq!(
            super::Caesar.decrypt(TEST_TEXT, &5),
            Some("Czggj cjr vmz tjp, oznodib, oznodib, 1 2 3".to_string()),
        );
    }

    #[test]
    fn encrypt() {
        assert_eq!(
            super::Caesar.encrypt(TEST_TEXT, &5),
            Some("Mjqqt mtb fwj dtz, yjxynsl, yjxynsl, 1 2 3".to_string()),
        );
    }

    #[test]
    fn crack() {
        assert_eq!(
            super::Caesar.crack("Czggj cjr vmz tjp, oznodib, oznodib, 1 2 3"),
            Some(TEST_TEXT.to_string()),
        );
    }
}
