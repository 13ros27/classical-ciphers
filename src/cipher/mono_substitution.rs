use super::{
    hill_climbing::{CanClimb, HillClimbing},
    utils::mono_substitution,
    Cipher, Crack,
};
use crate::analyse::Ngrams;
use rand::{distributions::Uniform, prelude::*, rngs::ThreadRng};

#[derive(Clone, Copy)]
pub struct MonoSubstitution;

impl Cipher for MonoSubstitution {
    type Key = Vec<usize>;

    fn decrypt(&self, mes: &str, key: &Vec<usize>) -> Option<String> {
        Some(mono_substitution(mes, &|c| key[c]))
    }

    fn encrypt(&self, mes: &str, key: &Vec<usize>) -> Option<String> {
        Some(mono_substitution(mes, &|c| {
            key.iter().position(|l| l == &c).unwrap()
        }))
    }
}

fn generate_initial_key() -> Vec<usize> {
    let mut rng = thread_rng();
    let mut initial_key = (0..26).collect::<Vec<_>>();
    initial_key.shuffle(&mut rng);
    initial_key
}

fn generate_new_key(rng: &mut ThreadRng, old_key: &Vec<usize>) -> Vec<usize> {
    let swap_indices: Vec<_> = rng
        .sample_iter(&Uniform::from(0..old_key.len()))
        .take(2)
        .collect();
    let mut new_key = old_key.to_owned();
    new_key.swap(swap_indices[0], swap_indices[1]);
    new_key
}

impl Crack for MonoSubstitution {
    fn crack(&self, mes: &str) -> Option<String> {
        // TODO: Most of the time is spent in the fitness function
        let quadgrams = Ngrams::new(4);
        Some(
            HillClimbing::new(
                mes.to_string(),
                generate_new_key,
                MonoSubstitution,
                &|q: &str| quadgrams.fitness(q),
            )
            .threaded()
            .loops(30)
            .climb(generate_initial_key),
        )
        // Some(threaded_hill_climbing(
        //     mes.to_string(),
        //     generate_initial_key,
        //     generate_new_key,
        //     MonoSubstitution,
        //     &|t: &str| quadgrams.fitness(t),
        //     ThreadedHillTweaks {
        //         loops: 30,
        //         ..Default::default()
        //     },
        // ))
    }
}
