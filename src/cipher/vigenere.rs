use super::{utils::periodic_force, Caesar, Cipher, KLCrack, KeySpace};
use crate::{
    analyse::mono_score,
    modify::{clean_verbose, expand, string_to_num},
};

#[derive(Clone, Copy)]
pub struct Vigenere;

fn crypt(mes: &str, key: &str, sub: bool) -> String {
    let (mes, expansion) = clean_verbose(mes);
    let num_mes = string_to_num(&mes);
    let num_key = string_to_num(key);
    let mut ciphertext = String::new();
    for (index, let_num) in num_mes.iter().enumerate() {
        let key_num = if sub {
            26 - num_key[index % num_key.len()]
        } else {
            num_key[index % num_key.len()]
        };
        ciphertext.push(((let_num + key_num) % 26 + 65) as u8 as char);
    }
    expand(&ciphertext, expansion)
}

impl Cipher for Vigenere {
    type Key = String;

    fn decrypt(&self, mes: &str, key: &String) -> Option<String> {
        Some(crypt(mes, key, true))
    }

    fn encrypt(&self, mes: &str, key: &String) -> Option<String> {
        Some(crypt(mes, key, false))
    }
}

impl KLCrack for Vigenere {
    fn kl_crack(&self, mes: &str, keylength: usize) -> Option<String> {
        Some(periodic_force(
            mes,
            &Caesar.keyspace(),
            &Caesar,
            &mono_score,
            keylength,
        ))
    }
}

#[cfg(test)]
mod tests {
    use crate::prelude::*;
    const TEST_TEXT: &str = "Hello how are you, testing, testing, 1 2 3";

    #[test]
    fn decrypt() {
        assert_eq!(
            super::Vigenere.decrypt(TEST_TEXT, &"Testing".to_string()),
            Some("Oatsg uid wzl qbo, aaaaaaa, aaaaaaa, 1 2 3".to_string()),
        );
    }

    #[test]
    fn encrypt() {
        assert_eq!(
            super::Vigenere.encrypt(TEST_TEXT, &"Testing".to_string()),
            Some("Aidew uup ejx gba, mikmqam, mikmqam, 1 2 3".to_string()),
        );
    }

    #[test]
    fn crack() {
        assert_eq!(
            super::Vigenere.crack("Kv Bhvtj 1937, qc alv yztjorim dm xyg Pxuhvpjjyk kjm UIM ukarvzvtms h wdcta wetmmi vj uqkjtieva. Ioip eigymvf i hdejvqzh eef bwl pvimck \"Hzg Iajlvoqhaie\".

Vpt weggzh htggiglh kq jt useumczi rpl llvv kocvvvf ccamc 1939, yptu mevmglwk kv psp kjqcnw Xgzbhr kqwz vr r pml zmxpqupgrpkt. H cfwvv prkgtapkvpkt vjwkkty grnttk Tyktdtiec Jahgb  yih aejmms dmkj ichppuqcn xygu qbx, nkbw alv eqeoii fmehvkomca jlntn lrxcotk gickzprx uqvuecu qcaicnqvlrtg ngvq Vwzdwi, jjm whh eq ztzsltktz xf ezpjo kjm rvhv jmgzich ick grnttk lvt ngpief Ppyvp hzdt xyg vtdpp hwgtiu Dcglel qn Hlgltqif eef Axnrrna Xuxvntxnieem. Hosiv aihjwgl puh zp i ghgv copprjv bxti, kjmn uivf gdbv ygte as stmpr xyg kxwlvta ..."),
            Some("In March 1937, in the wreckage of the Hindenburg the FBI discovered a small packet of documents. They carried a swastika and the legend \"Die Alchemisten\".

The papers appeared to be nonsense and were ignored until 1939, when interest in all things German took on a new significance. A young intelligence officer called Philomena Black  was tasked with analysing them but, with the cipher department fully engaged cracking signals intelligence from Europe, she had no resources to crack the code herself and called her friend Harry from the newly formed Bureau of Security and Signals Intelligence. Short staffed and in a race against time, they need your help to break the ciphers ...".to_string()),
        );
    }

    #[test]
    fn kl_crack() {
        assert_eq!(
            super::Vigenere.kl_crack("Kv Bhvtj 1937, qc alv yztjorim dm xyg Pxuhvpjjyk kjm UIM ukarvzvtms h wdcta wetmmi vj uqkjtieva. Ioip eigymvf i hdejvqzh eef bwl pvimck \"Hzg Iajlvoqhaie\".

Vpt weggzh htggiglh kq jt useumczi rpl llvv kocvvvf ccamc 1939, yptu mevmglwk kv psp kjqcnw Xgzbhr kqwz vr r pml zmxpqupgrpkt. H cfwvv prkgtapkvpkt vjwkkty grnttk Tyktdtiec Jahgb  yih aejmms dmkj ichppuqcn xygu qbx, nkbw alv eqeoii fmehvkomca jlntn lrxcotk gickzprx uqvuecu qcaicnqvlrtg ngvq Vwzdwi, jjm whh eq ztzsltktz xf ezpjo kjm rvhv jmgzich ick grnttk lvt ngpief Ppyvp hzdt xyg vtdpp hwgtiu Dcglel qn Hlgltqif eef Axnrrna Xuxvntxnieem. Hosiv aihjwgl puh zp i ghgv copprjv bxti, kjmn uivf gdbv ygte as stmpr xyg kxwlvta ...", 6),
            Some("In March 1937, in the wreckage of the Hindenburg the FBI discovered a small packet of documents. They carried a swastika and the legend \"Die Alchemisten\".

The papers appeared to be nonsense and were ignored until 1939, when interest in all things German took on a new significance. A young intelligence officer called Philomena Black  was tasked with analysing them but, with the cipher department fully engaged cracking signals intelligence from Europe, she had no resources to crack the code herself and called her friend Harry from the newly formed Bureau of Security and Signals Intelligence. Short staffed and in a race against time, they need your help to break the ciphers ...".to_string()),
        );
    }
}
