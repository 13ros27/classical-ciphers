use super::{
    utils::{brute_force, mono_substitution},
    Cipher, Crack, KeySpace,
};
use crate::{analyse::mono_score, maths::MMI_26};

#[derive(Clone, Copy)]
pub struct Affine;

impl KeySpace for Affine {
    fn keyspace(&self) -> Vec<(usize, usize)> {
        [1, 3, 5, 7, 9, 11, 15, 17, 19, 21, 23, 25]
            .iter()
            .flat_map(|a| (0..26).map(move |b| (*a, b)))
            .collect()
    }
}

impl Cipher for Affine {
    type Key = (usize, usize);

    fn decrypt(&self, mes: &str, key: &(usize, usize)) -> Option<String> {
        let inverse = match MMI_26.get(&((key.0 % 26) as u8)) {
            Some(value) => *value as usize,
            None => return None,
        };
        Some(mono_substitution(mes, &|x| {
            ((inverse * (x + 26 - (key.1 % 26))) % 26) as usize
        }))
    }

    fn encrypt(&self, mes: &str, key: &(usize, usize)) -> Option<String> {
        Some(mono_substitution(mes, &|x| (key.0 * x + key.1) % 26))
    }
}

impl Crack for Affine {
    fn crack(&self, mes: &str) -> Option<String> {
        Some(brute_force(mes, &self.keyspace(), &Self, &mono_score))
    }
}

#[cfg(test)]
mod tests {
    use crate::prelude::*;
    const TEST_TEXT: &str = "Hello how are you, testing, testing, 1 2 3";

    #[test]
    fn decrypt() {
        assert_eq!(
            super::Affine.decrypt(TEST_TEXT, &(3, 7)),
            Some("Azkkl alf pmz xln, ezvejcr, ezvejcr, 1 2 3".to_string()),
        );
    }

    #[test]
    fn encrypt() {
        assert_eq!(
            super::Affine.encrypt(TEST_TEXT, &(3, 7)),
            Some("Ctoox cxv hgt bxp, mtjmfuz, mtjmfuz, 1 2 3".to_string()),
        );
    }

    #[test]
    fn crack() {
        assert_eq!(
            super::Affine.crack("Czggj cjr vmz tjp, oznodib, oznodib, 1 2 3"),
            Some(TEST_TEXT.to_string()),
        );
    }
}
