use crate::{
    analyse::ASCII_LOWER,
    cipher::Cipher,
    modify::{clean_verbose, expand, rejoin, split},
    utils::BestScore,
};
use crossbeam_utils::thread;
use std::sync::{mpsc, Arc, Mutex};

pub fn mono_substitution(mes: &str, formula: &dyn Fn(usize) -> usize) -> String {
    let (mut clean_mes, expansion) = clean_verbose(mes);
    clean_mes.make_ascii_uppercase();
    let mut ciphertext = String::new();
    for letter in clean_mes.chars() {
        ciphertext.push(ASCII_LOWER[formula(letter as usize - 65)]);
    }
    expand(&ciphertext, expansion)
}

/// Attempts to brute force crack a given message with a decrypter and keyspace (keyspace must be valid)
pub fn brute_force<S: Copy + PartialOrd, T>(
    mes: &str,
    keys: &[T],
    cipher: &impl Cipher<Key = T>,
    fitness: &impl Fn(&str) -> S,
) -> String {
    let mut best = BestScore::new();
    for pos_key in keys {
        let pos_dec = cipher.decrypt(mes, pos_key).unwrap();
        best.add(fitness(&pos_dec.to_ascii_uppercase()), pos_dec);
    }
    best.arg
}

pub fn threaded_brute_force<S: Copy + PartialOrd + Send, T: Clone + Default + Sync>(
    mes: &str,
    keys: &[T],
    cipher: &(impl Cipher<Key = T> + Sync),
    fitness: &(impl Fn(&str) -> S + Sync),
    threads: usize,
) -> String {
    let key_counts = keys.len();
    let key_splits = keys
        .chunks((key_counts + (threads - 1)) / threads)
        .collect::<Vec<_>>();
    let mes = Arc::new(mes);
    let (tx, rx) = mpsc::channel();
    let tx = Arc::new(Mutex::new(tx));
    let cipher = Arc::new(cipher);
    let mut outer_best = BestScore::<String, _>::new();
    thread::scope(|s| {
        for key_split in key_splits {
            let (mes, tx, cipher) = (Arc::clone(&mes), tx.clone(), cipher.clone());
            s.spawn(move |_| {
                let mut inner_best = BestScore::new();
                for pos_key in key_split {
                    let pos_dec = cipher.decrypt(&mes, pos_key).unwrap();
                    inner_best.add(fitness(&pos_dec.to_ascii_uppercase()), pos_dec);
                }
                tx.lock()
                    .unwrap()
                    .send((inner_best.score(), inner_best.arg))
                    .unwrap();
            });
        }
        for _ in 0..threads {
            let (score, arg) = rx.recv().unwrap();
            outer_best.add(score, arg);
        }
    })
    .unwrap();
    outer_best.arg
}

/// Attempts to crack a given polyalphabetic message by splitting it up and cracking it part by part
pub fn periodic_force<S: Copy + PartialOrd, T>(
    mes: &str,
    keys: &[T],
    cipher: &impl Cipher<Key = T>,
    fitness: &impl Fn(&str) -> S,
    keylength: usize,
) -> String {
    let (clean_mes, expansion) = clean_verbose(mes);
    let split_crack = split(&clean_mes, keylength)
        .iter()
        .map(|t| brute_force(t, keys, cipher, fitness))
        .collect::<Vec<String>>();
    expand(&rejoin(split_crack), expansion)
}

#[cfg(test)]
mod tests {
    const TEST_TEXT: &str = "Hello how are you, testing, testing, 1 2 3";

    #[test]
    fn mono_substitution() {
        assert_eq!(
            super::mono_substitution(TEST_TEXT, &|x| ((x as i32 * 3 + 5) % 26) as usize),
            "Armmv avt fer zvn, krhkdsx, krhkdsx, 1 2 3",
        );
    }
}
