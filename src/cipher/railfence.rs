use super::{utils::brute_force, Cipher, Crack, KeySpace};
use crate::{analyse::Ngrams, modify::clean};

#[derive(Clone, Copy)]
pub struct Railfence;

impl KeySpace for Railfence {
    fn keyspace(&self) -> Vec<usize> {
        (2..11).collect()
    }
}

impl Cipher for Railfence {
    type Key = usize;

    fn decrypt(&self, mes: &str, key: &usize) -> Option<String> {
        let mes = clean(mes);
        let mut fill_spots = vec![vec![false; mes.len()]; *key];
        let mut lines = vec![vec![None; mes.len()]; *key];
        let chars = mes.chars().collect::<Vec<_>>();
        let mut dir_down = true;
        let (mut row, mut col) = (0, 0);
        for _ in 0..mes.len() {
            if row == 0 || row == key - 1 {
                dir_down = !dir_down;
            }
            fill_spots[row][col] = true;
            col += 1;
            if dir_down {
                row -= 1;
            } else {
                row += 1;
            }
        }
        let mut index = 0;
        for i in 0..*key {
            for j in 0..mes.len() {
                if fill_spots[i][j] && (index < mes.len()) {
                    lines[i][j] = Some(chars[index]);
                    index += 1;
                }
            }
        }
        let mut result = Vec::new();
        let (mut row, mut col) = (0, 0);
        let mut dir_down = true;
        for _ in 0..mes.len() {
            if row == 0 || row == key - 1 {
                dir_down = !dir_down;
            }
            if let Some(c) = lines[row][col] {
                result.push(c);
                col += 1;
            }
            if dir_down {
                row -= 1;
            } else {
                row += 1;
            }
        }
        Some(result.iter().collect::<String>())
    }

    fn encrypt(&self, mes: &str, key: &usize) -> Option<String> {
        let mes = clean(mes);
        let mut lines = vec![Vec::new(); *key];
        let mut line_num = 0;
        let mut going_up = true;
        for letter in mes.chars() {
            lines[line_num].push(letter);
            if going_up {
                line_num += 1;
            } else {
                line_num -= 1;
            }
            if line_num == key - 1 || line_num == 0 {
                going_up = !going_up;
            }
        }
        Some(lines.iter().flatten().collect::<String>())
    }
}

impl Crack for Railfence {
    fn crack(&self, mes: &str) -> Option<String> {
        let bigrams = Ngrams::new(2);
        Some(brute_force(mes, &self.keyspace(), &Self, &|t: &str| {
            bigrams.fitness(t)
        }))
    }
}
