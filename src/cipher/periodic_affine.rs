use super::{utils::periodic_force, Affine, Cipher, KLCrack, KeySpace};
use crate::{
    analyse::mono_score,
    modify::{clean_verbose, expand, rejoin, split},
};

#[derive(Clone, Copy)]
pub struct PeriodicAffine;

impl Cipher for PeriodicAffine {
    type Key = Vec<(usize, usize)>;

    fn decrypt(&self, mes: &str, key: &Vec<(usize, usize)>) -> Option<String> {
        let (mes, expansion) = clean_verbose(mes);
        Some(expand(
            &rejoin(
                split(&mes, key.len())
                    .iter()
                    .enumerate()
                    .map(|(i, p)| Affine.decrypt(p, &key[i]).unwrap())
                    .collect(),
            ),
            expansion,
        ))
    }

    fn encrypt(&self, mes: &str, key: &Vec<(usize, usize)>) -> Option<String> {
        let (mes, expansion) = clean_verbose(mes);
        Some(expand(
            &rejoin(
                split(&mes, key.len())
                    .iter()
                    .enumerate()
                    .map(|(i, p)| Affine.encrypt(p, &key[i]).unwrap())
                    .collect(),
            ),
            expansion,
        ))
    }
}

impl KLCrack for PeriodicAffine {
    fn kl_crack(&self, mes: &str, keylength: usize) -> Option<String> {
        Some(periodic_force(
            mes,
            &Affine.keyspace(),
            &Affine,
            &mono_score,
            keylength,
        ))
    }
}
