use super::{
    hill_climbing::{CanClimb, HillClimbing},
    Cipher, Crack, KLCrack,
};
use crate::{
    analyse::Ngrams,
    modify::{clean_verbose, expand, rejoin, split},
    utils::BestScore,
};
use rand::{distributions::Uniform, prelude::*, rngs::ThreadRng};

fn crypt(mes: &str, key: &[usize]) -> Option<String> {
    let (clean_mes, expansion) = clean_verbose(mes);
    if clean_mes.len() % key.len() != 0 {
        return None;
    }
    let split_texts = split(&clean_mes, key.len());
    let mut end_texts = Vec::new();
    for num in key {
        end_texts.push(split_texts[*num].clone());
    }
    Some(expand(&rejoin(end_texts), expansion))
}

#[derive(Clone, Copy)]
pub struct Permutation;

impl Cipher for Permutation {
    type Key = Vec<usize>;

    fn decrypt(&self, mes: &str, key: &Vec<usize>) -> Option<String> {
        let mut nkey = vec![0; key.len()];
        for (i, num) in key.iter().enumerate() {
            nkey[*num] = i;
        }
        crypt(mes, &nkey)
    }

    fn encrypt(&self, mes: &str, key: &Vec<usize>) -> Option<String> {
        crypt(mes, key)
    }
}

#[allow(clippy::ptr_arg)] // Because this use relates to generics
fn generate_new_key(rng: &mut ThreadRng, old_key: &Vec<usize>) -> Vec<usize> {
    let swap_indices: Vec<_> = rng
        .sample_iter(&Uniform::from(0..old_key.len()))
        .take(2)
        .collect();
    let mut new_key = old_key.to_owned();
    new_key.swap(swap_indices[0], swap_indices[1]);
    new_key
}

fn generate_initial_key(keylength: usize) -> Vec<usize> {
    let mut rng = thread_rng();
    let mut initial_key = (0..keylength).collect::<Vec<_>>();
    initial_key.shuffle(&mut rng);
    initial_key
}

impl KLCrack for Permutation {
    fn kl_crack(&self, mes: &str, keylength: usize) -> Option<String> {
        let trigrams = Ngrams::new(3);
        Some(
            HillClimbing::new(
                mes.to_string(),
                generate_new_key,
                Permutation,
                &|t: &str| trigrams.fitness(t),
            )
            .threaded()
            .section_length(300)
            .climb(|| generate_initial_key(keylength), keylength),
        )
        // Some(threaded_hill_climbing(
        //     mes.to_string(),
        //     || generate_initial_key(keylength),
        //     generate_new_key,
        //     Permutation,
        //     &|t: &str| trigrams.fitness(t),
        //     ThreadedHillTweaks {
        //         section_length: Some(300),
        //         keylength,
        //         ..Default::default()
        //     },
        // ))
    }
}

impl Crack for Permutation {
    fn crack(&self, mes: &str) -> Option<String> {
        let (mes, expansion) = clean_verbose(mes);
        let mut best = BestScore::new();
        let bigrams = Ngrams::new(2);
        for pos_kl in 2..10 {
            if mes.len() % pos_kl != 0 {
                continue;
            }
            let pos_dec = self.kl_crack(&mes, pos_kl).unwrap();
            let fitness = bigrams.fitness(&pos_dec);
            best.add(fitness, pos_dec);
            if fitness > 28000000 {
                break;
            }
        }
        Some(expand(&best.arg, expansion))
    }
}

#[cfg(test)]
mod tests {
    use crate::prelude::*;
    const TEST_TEXT: &str = "Hello how are you, testing, testing, 1 2 3";

    #[test]
    fn decrypt() {
        assert_eq!(
            super::Permutation.decrypt(
                "Hlleo owh aey rot, eusintg, esttngi, 1 2 3",
                &vec![0, 2, 3, 1]
            ),
            Some(TEST_TEXT.to_string()),
        );
    }

    #[test]
    fn encrypt() {
        assert_eq!(
            super::Permutation.encrypt(TEST_TEXT, &vec![0, 2, 3, 1]),
            Some("Hlleo owh aey rot, eusintg, esttngi, 1 2 3".to_string()),
        );
    }

    #[test]
    fn crack() {
        assert_eq!(
            super::Permutation.crack("Ci Rname 1937, hh itn awkrcehg te foe Edhnitngbr udh IEB feivsocmrs e adcaa lpldkf eo tnoecmuyt. Esht eciarrs d aawsntai ake dlt ehigde \"Dne Ehaclnmeits\".\n\nPta hpepep rastedae rn oo bneasees nen rdew eirgonl diutn 1939, nwih nettseer ti lnl aghsig Ntenra maon oo k gni eswanciifony. C aetun nigeeglilfnf coeaicc rehlpl Denielmok Acbal  swa atstki ewdy hlaanhsti gnwe tmu, beih tth rceihp tdreapumfe tngln leyracg degancik lsaingl seitnclniege emfo Rsuerp, onh dea hu oorseortc se tckrc aeh deoc lheesra fca dnelhl den refiry Drhra hftr moy elnwe dfeomr Ubauer uo Cfesnrai yta Dnsgiel Tsninlelgirc. Oehsf tfsatie ddn ae n caar sangiat tetm, iehe enyh dryu obeo lt phrte kae ehcpixr ...xsxx"),
            Some("In March 1937, in the wreckage of the Hindenburg the FBI discovered a small packet of documents. They carried a swastika and the legend \"Die Alchemisten\".\n\nThe papers appeared to be nonsense and were ignored until 1939, when interest in all things German took on a new significance. A young intelligence officer called Philomena Black  was tasked with analysing them but, with the cipher department fully engaged cracking signals intelligence from Europe, she had no resources to crack the code herself and called her friend Harry from the newly formed Bureau of Security and Signals Intelligence. Short staffed and in a race against time, they need your help to break the ciphers ...xxxx".to_string()),
        );
    }
}
