use super::{Cipher, Crack, KLCrack};
use crate::analyse::{is_polybius, ASCII_LOWER};
use std::collections::HashMap;

#[derive(Clone, Copy)]
pub struct Polybius;

impl Cipher for Polybius {
    type Key = HashMap<String, char>;

    fn decrypt(&self, _mes: &str, _key: &HashMap<String, char>) -> Option<String> {
        todo!()
    }

    fn encrypt(&self, _mes: &str, _key: &HashMap<String, char>) -> Option<String> {
        todo!()
    }
}

impl KLCrack for Polybius {
    fn kl_crack(&self, mes: &str, block_length: usize) -> Option<String> {
        let mes = mes.replace(' ', "").replace('\n', "");
        if mes.len() % block_length != 0 {
            return None;
        }
        let chars = mes.chars().collect::<Vec<_>>();
        let pairings = chars.chunks(block_length).collect::<Vec<_>>();
        let mut available_pairs = pairings.clone();
        available_pairs.sort();
        available_pairs.dedup();
        if available_pairs.len() > 26 {
            return None;
        }
        Some(
            pairings
                .iter()
                .map(|p| ASCII_LOWER[available_pairs.iter().position(|l| l == p).unwrap()])
                .collect::<String>(),
        )
    }
}

impl Crack for Polybius {
    fn crack(&self, mes: &str) -> Option<String> {
        for block_length in 2..6 {
            if is_polybius(mes, block_length) {
                return Self.kl_crack(mes, block_length);
            }
        }
        None
    }
}
