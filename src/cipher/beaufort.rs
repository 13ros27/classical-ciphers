use super::{
    utils::{mono_substitution, periodic_force},
    Caesar, Cipher, KLCrack, KeySpace,
};
use crate::{
    analyse::mono_score,
    modify::{clean_verbose, expand, string_to_num},
};

#[derive(Clone, Copy)]
pub struct Beaufort;

fn crypt(mes: &str, key: &str) -> String {
    let (mes, expansion) = clean_verbose(mes);
    let num_mes = string_to_num(&mes);
    let num_key = string_to_num(key);
    let mut ciphertext = String::new();
    for (index, let_num) in num_mes.iter().enumerate() {
        let key_num = num_key[index % num_key.len()];
        ciphertext.push(((26 + key_num - let_num) % 26 + 65) as u8 as char);
    }
    expand(&ciphertext, expansion)
}

impl Cipher for Beaufort {
    type Key = String;

    fn decrypt(&self, mes: &str, key: &String) -> Option<String> {
        Some(crypt(mes, key))
    }

    fn encrypt(&self, mes: &str, key: &String) -> Option<String> {
        Some(crypt(mes, key))
    }
}

impl KLCrack for Beaufort {
    fn kl_crack(&self, mes: &str, keylength: usize) -> Option<String> {
        Some(periodic_force(
            mes,
            &Caesar.keyspace(),
            &BeaufortPart,
            &mono_score,
            keylength,
        ))
    }
}

#[derive(Clone, Copy)]
struct BeaufortPart;

impl Cipher for BeaufortPart {
    type Key = usize;

    fn decrypt(&self, mes: &str, key: &usize) -> Option<String> {
        Some(mono_substitution(mes, &|x| ((key + 26 - x) % 26)))
    }

    fn encrypt(&self, mes: &str, key: &usize) -> Option<String> {
        self.decrypt(mes, key)
    }
}

#[cfg(test)]
mod tests {
    use crate::prelude::*;
    const TEST_TEXT: &str = "Hello how are you, testing, testing, 1 2 3";

    #[test]
    fn decrypt() {
        assert_eq!(
            super::Beaufort.decrypt(TEST_TEXT, &"Testing".to_string()),
            Some("Mahiu gsx ebp kzm, aaaaaaa, aaaaaaa, 1 2 3".to_string()),
        );
    }

    #[test]
    fn encrypt() {
        assert_eq!(
            super::Beaufort.encrypt(TEST_TEXT, &"Testing".to_string()),
            Some("Mahiu gsx ebp kzm, aaaaaaa, aaaaaaa, 1 2 3".to_string()),
        );
    }

    #[test]
    fn crack() {
        assert_eq!(
            super::Beaufort.crack("Lr Gtrlz 1937, lr zme rppcitcj so llp Bftqafsowa axo OHF dlmqfnjppb s bwnvi psryjn fz pfgtuprzb. Pgcv cscrfcq e axivnlus tvk nma hpcjtq \"Bkp Icemaglqucg\".

Llp tnrpna ttyctnoq pz fp regqjtba sgf rcca knvzppb ygpfv 1939, xxog aanpnobp ft tth abftnm Mprbgg lefy zt t rox qfagwnlgntra. S vuttn wfaecvlyoggj sozkrew etthpf Yzlteheag Stsry  rgb lsbyjd xwzm iagigalvh nmag sou, klll abj elplpr kceebawjta zyixp cgysnek eceqjaaa bwmgico lrzpxcynafre ipfs Ozrzrp, mlp bnd gq bpqzmccob pz eceqj pgc rqpp bjpbaho iad rehiek zpn ncajtq Xscrp bcqg abj tpihv dzphap Sowctk eo Qjeznkak ntq Mknvnvb Wfaecvlyoggj. Omqba qugozoq iad lr s cilc tyslvvn awgp, pgcv ropf pszn lpxy nf dbpid nma qltgccm ..."),
            Some("In March 1937, in the wreckage of the Hindenburg the FBI discovered a small packet of documents. They carried a swastika and the legend \"Die Alchemisten\".

The papers appeared to be nonsense and were ignored until 1939, when interest in all things German took on a new significance. A young intelligence officer called Philomena Black  was tasked with analysing them but, with the cipher department fully engaged cracking signals intelligence from Europe, she had no resources to crack the code herself and called her friend Harry from the newly formed Bureau of Security and Signals Intelligence. Short staffed and in a race against time, they need your help to break the ciphers ...".to_string()),
        );
    }

    #[test]
    fn kl_crack() {
        assert_eq!(
            super::Beaufort.kl_crack("Lr Gtrlz 1937, lr zme rppcitcj so llp Bftqafsowa axo OHF dlmqfnjppb s bwnvi psryjn fz pfgtuprzb. Pgcv cscrfcq e axivnlus tvk nma hpcjtq \"Bkp Icemaglqucg\".

Llp tnrpna ttyctnoq pz fp regqjtba sgf rcca knvzppb ygpfv 1939, xxog aanpnobp ft tth abftnm Mprbgg lefy zt t rox qfagwnlgntra. S vuttn wfaecvlyoggj sozkrew etthpf Yzlteheag Stsry  rgb lsbyjd xwzm iagigalvh nmag sou, klll abj elplpr kceebawjta zyixp cgysnek eceqjaaa bwmgico lrzpxcynafre ipfs Ozrzrp, mlp bnd gq bpqzmccob pz eceqj pgc rqpp bjpbaho iad rehiek zpn ncajtq Xscrp bcqg abj tpihv dzphap Sowctk eo Qjeznkak ntq Mknvnvb Wfaecvlyoggj. Omqba qugozoq iad lr s cilc tyslvvn awgp, pgcv ropf pszn lpxy nf dbpid nma qltgccm ...", 7),
            Some("In March 1937, in the wreckage of the Hindenburg the FBI discovered a small packet of documents. They carried a swastika and the legend \"Die Alchemisten\".

The papers appeared to be nonsense and were ignored until 1939, when interest in all things German took on a new significance. A young intelligence officer called Philomena Black  was tasked with analysing them but, with the cipher department fully engaged cracking signals intelligence from Europe, she had no resources to crack the code herself and called her friend Harry from the newly formed Bureau of Security and Signals Intelligence. Short staffed and in a race against time, they need your help to break the ciphers ...".to_string()),
        );
    }
}
