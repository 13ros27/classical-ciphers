use crate::{cipher::Cipher, modify::clean, utils::BestScore};
use crossbeam_utils::thread;
use rand::{prelude::*, thread_rng};
use std::marker::PhantomData;
use std::sync::{mpsc, Arc, Mutex};

pub trait CanClimb<T: Default, I: Fn() -> T> {
    fn key(&self, initial_key_gen: I) -> T;
    fn climb(&self, initial_key_gen: I) -> String;
    fn section_length(self, length: usize) -> SLHillClimbing<T, I, Self>
    where
        Self: Sized,
    {
        SLHillClimbing {
            hill_climbing: self,
            section_length: length,
            _phantom: Default::default(),
        }
    }
}

pub trait ClimbMes<T, I> {
    fn mes(&self) -> String;
    fn decrypt(&self, mes: String, key: T) -> String;
    fn key_with_mes(&self, mes: String, initial_key_gen: I) -> T;
}

pub struct HillClimbing<T, N, C, F, I>
where
    T: Default,
    N: Fn(&mut ThreadRng, &T) -> T,
    C: Cipher<Key = T>,
    F: Fn(&str) -> i64,
    I: Fn() -> T,
{
    mes: String,
    new_key_gen: N,
    cipher: C,
    fitness: F,
    max_without_change: usize,
    _phantom: PhantomData<I>,
}

impl<T, N, C, F, I> HillClimbing<T, N, C, F, I>
where
    T: Default,
    N: Fn(&mut ThreadRng, &T) -> T,
    C: Cipher<Key = T>,
    F: Fn(&str) -> i64,
    I: Fn() -> T,
{
    pub fn new(mes: String, new_key_gen: N, cipher: C, fitness: F) -> Self {
        Self {
            mes,
            new_key_gen,
            cipher,
            fitness,
            max_without_change: 100,
            _phantom: Default::default(),
        }
    }

    pub fn without_change(mut self, max: usize) -> Self {
        self.max_without_change = max;
        self
    }

    pub fn threaded(self) -> ThreadHillClimbing<T, N, C, F, I> {
        ThreadHillClimbing {
            single_threaded: self,
            threads: 12,
            loops: 1,
        }
    }
}

pub struct ThreadHillClimbing<T, N, C, F, I>
where
    T: Default,
    N: Fn(&mut ThreadRng, &T) -> T,
    C: Cipher<Key = T>,
    F: Fn(&str) -> i64,
    I: Fn() -> T,
{
    single_threaded: HillClimbing<T, N, C, F, I>,
    threads: usize,
    loops: usize,
}

impl<T, N, C, F, I> ThreadHillClimbing<T, N, C, F, I>
where
    T: Default,
    N: Fn(&mut ThreadRng, &T) -> T,
    C: Cipher<Key = T>,
    F: Fn(&str) -> i64,
    I: Fn() -> T,
{
    pub fn without_change(mut self, max: usize) -> Self {
        self.single_threaded.max_without_change = max;
        self
    }

    pub fn threads(mut self, threads: usize) -> Self {
        self.threads = threads;
        self
    }

    pub fn loops(mut self, loops: usize) -> Self {
        self.loops = loops;
        self
    }
}

impl<T, N, C, F, I> CanClimb<T, I> for HillClimbing<T, N, C, F, I>
where
    T: Default,
    N: Fn(&mut ThreadRng, &T) -> T,
    C: Cipher<Key = T>,
    F: Fn(&str) -> i64,
    I: Fn() -> T,
{
    fn key(&self, initial_key_gen: I) -> T {
        self.key_with_mes(self.mes(), initial_key_gen)
    }

    fn climb(&self, initial_key_gen: I) -> String {
        self.cipher
            .decrypt(&self.mes, &self.key(initial_key_gen))
            .unwrap()
    }
}

impl<T, N, C, F, I> CanClimb<T, I> for ThreadHillClimbing<T, N, C, F, I>
where
    T: Default + Send + Sync,
    N: Fn(&mut ThreadRng, &T) -> T + Send + Sync,
    C: Cipher<Key = T> + Send + Sync,
    F: Fn(&str) -> i64 + Send + Sync,
    I: Fn() -> T + Copy + Send + Sync,
{
    fn key(&self, initial_key_gen: I) -> T {
        self.key_with_mes(self.mes(), initial_key_gen)
    }

    fn climb(&self, initial_key_gen: I) -> String {
        self.single_threaded
            .cipher
            .decrypt(&self.single_threaded.mes, &self.key(initial_key_gen))
            .unwrap()
    }
}

impl<T, N, C, F, I> ClimbMes<T, I> for HillClimbing<T, N, C, F, I>
where
    T: Default,
    N: Fn(&mut ThreadRng, &T) -> T,
    C: Cipher<Key = T>,
    F: Fn(&str) -> i64,
    I: Fn() -> T,
{
    fn mes(&self) -> String {
        self.mes.clone()
    }

    fn decrypt(&self, mes: String, key: T) -> String {
        self.cipher.decrypt(&mes, &key).unwrap()
    }

    fn key_with_mes(&self, mes: String, initial_key_gen: I) -> T {
        let mut rng = thread_rng();
        let mes = clean(&mes);
        let mut key = initial_key_gen();
        let mut best = BestScore::new();
        loop {
            best.add(
                (self.fitness)(&self.cipher.decrypt(&mes, &key).unwrap()),
                key,
            );
            key = (self.new_key_gen)(&mut rng, &best.arg);
            if best.last_changed > self.max_without_change {
                break;
            }
        }
        best.arg
    }
}

impl<T, N, C, F, I> ClimbMes<T, I> for ThreadHillClimbing<T, N, C, F, I>
where
    T: Default + Send + Sync,
    N: Fn(&mut ThreadRng, &T) -> T + Send + Sync,
    C: Cipher<Key = T> + Send + Sync,
    F: Fn(&str) -> i64 + Send + Sync,
    I: Fn() -> T + Copy + Send + Sync,
{
    fn mes(&self) -> String {
        self.single_threaded.mes()
    }

    fn decrypt(&self, mes: String, key: T) -> String {
        self.single_threaded.cipher.decrypt(&mes, &key).unwrap()
    }

    fn key_with_mes(&self, mes: String, initial_key_gen: I) -> T {
        let mes = Arc::new(mes);
        let (tx, rx) = mpsc::channel();
        let tx = Arc::new(Mutex::new(tx));
        let cipher = Arc::new(self.single_threaded.cipher);
        let mut outer_best = BestScore::<T, _>::new();
        thread::scope(|s| {
            for _ in 0..self.threads {
                let (mes, tx, cipher) = (Arc::clone(&mes), tx.clone(), cipher.clone());
                s.spawn(move |_| {
                    let mut inner_best = BestScore::new();
                    for _ in 0..self.loops {
                        let pos_key = self.single_threaded.key(initial_key_gen);
                        let pos_dec = cipher.decrypt(&mes, &pos_key).unwrap();
                        let fit = (self.single_threaded.fitness)(&pos_dec);
                        inner_best.add(fit, pos_key);
                        if fit > 600000 {
                            break;
                        }
                    }
                    tx.lock()
                        .unwrap()
                        .send((inner_best.score(), inner_best.arg))
                        .unwrap();
                });
            }
            for _ in 0..self.threads {
                let (score, arg) = rx.recv().unwrap();
                outer_best.add(score, arg);
            }
        })
        .unwrap();
        outer_best.arg
    }
}

pub struct SLHillClimbing<T: Default, I: Fn() -> T, H: CanClimb<T, I>> {
    hill_climbing: H,
    section_length: usize,
    _phantom: PhantomData<(T, I)>,
}

impl<T, I, H> SLHillClimbing<T, I, H>
where
    T: Default,
    I: Fn() -> T,
    H: CanClimb<T, I> + ClimbMes<T, I>,
{
    pub fn climb(&self, initial_key_gen: I, keylength: usize) -> String {
        let mut cut_mes = clean(&self.hill_climbing.mes());
        let sl = self.section_length;
        cut_mes.truncate(sl + (keylength - (sl % keylength)));
        self.hill_climbing.decrypt(
            self.hill_climbing.mes(),
            self.hill_climbing.key_with_mes(cut_mes, initial_key_gen),
        )
    }
}

impl<T, N, C, F, I> SLHillClimbing<T, I, HillClimbing<T, N, C, F, I>>
where
    T: Default,
    N: Fn(&mut ThreadRng, &T) -> T,
    C: Cipher<Key = T>,
    F: Fn(&str) -> i64,
    I: Fn() -> T,
{
    pub fn without_change(mut self, max: usize) -> Self {
        self.hill_climbing.max_without_change = max;
        self
    }
}

impl<T, N, C, F, I> SLHillClimbing<T, I, HillClimbing<T, N, C, F, I>>
where
    T: Default + Send + Sync,
    N: Fn(&mut ThreadRng, &T) -> T + Send + Sync,
    C: Cipher<Key = T> + Send + Sync,
    F: Fn(&str) -> i64 + Send + Sync,
    I: Fn() -> T + Copy + Send + Sync,
{
    pub fn threaded(self) -> SLHillClimbing<T, I, ThreadHillClimbing<T, N, C, F, I>> {
        SLHillClimbing {
            hill_climbing: ThreadHillClimbing {
                single_threaded: self.hill_climbing,
                threads: 12,
                loops: 1,
            },
            section_length: self.section_length,
            _phantom: Default::default(),
        }
    }
}

impl<T, N, C, F, I> SLHillClimbing<T, I, ThreadHillClimbing<T, N, C, F, I>>
where
    T: Default + Send + Sync,
    N: Fn(&mut ThreadRng, &T) -> T + Send + Sync,
    C: Cipher<Key = T> + Send + Sync,
    F: Fn(&str) -> i64 + Send + Sync,
    I: Fn() -> T + Copy + Send + Sync,
{
    pub fn without_change(mut self, max: usize) -> Self {
        self.hill_climbing.single_threaded.max_without_change = max;
        self
    }

    pub fn threads(mut self, threads: usize) -> Self {
        self.hill_climbing.threads = threads;
        self
    }

    pub fn loops(mut self, loops: usize) -> Self {
        self.hill_climbing.loops = loops;
        self
    }
}
