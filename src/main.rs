#![allow(dead_code)]

#[allow(unused_imports)]
use classical_ciphers::{prelude::*, *};
use std::collections::HashMap;

/// Parse a ciphertext off the NCC website
fn parse(challenge: u32, rescan: Option<usize>) -> (String, String) {
    let challenge_type = {
        if challenge > 3 {
            "competition"
        } else {
            "practice"
        }
    };
    let url = format!(
        "https://www.cipherchallenge.org/challenge/{}-challenge-{}/",
        challenge_type, challenge
    );
    let text = reqwest::blocking::get(&url).unwrap().text().unwrap();
    if let Some(value) = rescan {
        let delay = std::time::Duration::new(value as u64, 0);
        if text.contains("<title>Challenges - National Cipher Challenge ") {
            println!("Rescanning challenge {}...", challenge);
            std::thread::sleep(delay);
            return parse(challenge, rescan);
        }
    } else if text.contains("<title>Challenges - National Cipher Challenge ") {
        panic!("Cannot find page");
    }
    let div_loc = text.find("<div class=\"challenge__content a\"").unwrap();
    let start_loc = text
        .chars()
        .skip(div_loc)
        .collect::<String>()
        .find("<p>")
        .unwrap()
        + 3
        + div_loc;
    let end_loc = text
        .chars()
        .skip(start_loc)
        .collect::<String>()
        .find("</div>")
        .unwrap()
        - 1
        + start_loc;
    let cipher_a = &text[start_loc..end_loc]
        .replace("<p>", "")
        .replace("</p>", "")
        .replace("<br />", "");
    let div_loc = text.find("<div class=\"challenge__content\"").unwrap();
    let start_loc = text
        .chars()
        .skip(div_loc)
        .collect::<String>()
        .find("<p>")
        .unwrap()
        + 3
        + div_loc;
    let end_loc = text
        .chars()
        .skip(start_loc)
        .collect::<String>()
        .find("</div>")
        .unwrap()
        + start_loc;
    let cipher_b = &text[start_loc..end_loc]
        .replace("<p>", "")
        .replace("</p>", "")
        .replace("<br />", "");
    (cipher_a.trim().to_string(), cipher_b.trim().to_string())
}

fn submit(challenge: u32, part_a: bool, mes: &str) {
    let challenge_type = {
        if challenge > 3 {
            "competition"
        } else {
            "practice"
        }
    };
    let challenge_source = format!("/challenge/{}-challenge-{}/", challenge_type, challenge);
    let challenge_letter = format!("challenge_{}", if part_a { "a" } else { "b" });
    let cookies1 = "_ga=GA1.2.1248944430.1630252708; cookielawinfo-checkbox-necessary=yes; cookielawinfo-checkbox-non-necessary=yes; _gid=GA1.2.1672704078.1632143073; CookieLawInfoConsent=eyJuZWNlc3NhcnkiOnRydWUsIm5vbi1uZWNlc3NhcnkiOnRydWV9; viewed_cookie_policy=yes; wordpress_logged_in_eda8320ce3c100ebaae6446ba1812dc0=testautomation%7C1633790748%7CBuH2gH2g6uSCNcuMtOrhJyFaBVSr8VsDdz45AtzDmIq%7C63926bb4d010e019c70fa716db8c3d1615ec0326c5bbebed3b5f8b15aeb77475; _gat_gtag_UA_85099311_1=1";
    let cookies2 = "wordpress_sec_eda8320ce3c100ebaae6446ba1812dc0=testautomation%7C1633789323%7Cx7srCBzbQ1f2PA0hgFU4U2db0xsUUJfe0W1xN8x2Vyn%7Cb3e11bb8c56aa5913eac74d09c04347ee32b32de6aafb33ff3951e4e3a8a5a66; _ga=GA1.2.1248944430.1630252708; cookielawinfo-checkbox-necessary=yes; cookielawinfo-checkbox-non-necessary=yes; _gid=GA1.2.1672704078.1632143073; CookieLawInfoConsent=eyJuZWNlc3NhcnkiOnRydWUsIm5vbi1uZWNlc3NhcnkiOnRydWV9; viewed_cookie_policy=yes; wordpress_logged_in_eda8320ce3c100ebaae6446ba1812dc0=testautomation%7C1633789323%7Cx7srCBzbQ1f2PA0hgFU4U2db0xsUUJfe0W1xN8x2Vyn%7Cfe7f20619124d4275f2e96f3b68e625c3394e49fa1bee4592702fa956ac9a8c3; _gat_gtag_UA_85099311_1=1";
    let submission_box_text = reqwest::blocking::Client::new()
        .get(&format!(
            "https://www.cipherchallenge.org{}",
            challenge_source
        ))
        .header(reqwest::header::COOKIE, cookies1)
        .send()
        .unwrap()
        .text()
        .unwrap();
    let nonce_start = submission_box_text
        .find(&format!(
            "<input type=\"hidden\" name=\"td_submit_{}_nonce\" value=\"",
            challenge_letter
        ))
        .unwrap()
        + 63;
    let nonce = &submission_box_text[nonce_start..(nonce_start + 10)];
    println!("{:?}", nonce);
    let challenge_id_start = submission_box_text
        .find("<input type=\"hidden\" name=\"challenge_id\" value=\"")
        .unwrap()
        + 48;
    let challenge_id = &submission_box_text[challenge_id_start..(challenge_id_start + 5)];
    println!("{:?}", challenge_id);
    let mut json_content = HashMap::new();
    json_content.insert("action", format!("td_submit_{}", challenge_letter));
    let td_submit = &format!("td_submit_{}_nonce", challenge_letter);
    json_content.insert(td_submit, nonce.to_string());
    json_content.insert("_wp_http_referer", challenge_source);
    json_content.insert("answer", modify::clean(mes));
    json_content.insert("challenge_id", challenge_id.to_string());
    println!("{:?}", json_content);
    println!(
        "{:?}",
        reqwest::blocking::Client::new()
            .post("https://www.cipherchallenge.org/wp-admin/admin-ajax.php")
            .json(&json_content)
            .header(reqwest::header::COOKIE, cookies2)
            .header(reqwest::header::EXPIRES, "Thu, 07 Oct 2021 16:00:00 GMT")
            .send()
            .unwrap()
    );
}

fn full_auto_crack(challenge: u32, rescan: Option<usize>) -> (String, String) {
    let (cipher_a, cipher_b) = parse(challenge, rescan);
    let ((crack_a, _), (crack_b, _)) = (crack!(&cipher_a), crack!(&cipher_b));
    submit(challenge, true, &crack_a);
    submit(challenge, false, &crack_b);
    (crack_a, crack_b)
}

fn read_previous_ciphers(name: &str) -> Vec<String> {
    let data = std::fs::read_to_string(format!("previous_ciphers/{}.json", name))
        .expect("Unable to read file");
    serde_json::from_str(&data).expect("JSON was not well-formatted")
}

// use classical_ciphers::csl::ide;

fn main() {
    // ide();
    // for cipher in read_previous_ciphers("2021") {
    //     let (plaintext, cipher) = crack!(&cipher);
    //     println!("{}: {}", cipher, plaintext);
    // }
    let message = "VRDPK RWFAO MMPRZ EVEWE UTCOP LHTAT GELUI ITOAT XEZRV NRTTF HUDDB TBOAT AIVRD PKRWW USAOU MKSWI RNEDR YLHWC ZRTAF OGEFO WDLHI OVTAO SEYFL HQCYM AIASB OVIGG QNCAB TDVBT WETOA TWEIO XTGRW OGNFF KCTLR TNAWD AOKAV EGEAO QMQNT AWIFN BOBIV SZEPE DOQMG NFAXT PREEJ RTOXP BOIEZ RTNKW BRZBF FRRDD YYQNH OLHJR ATHEZ EPEDO QMGNT OATME VRQXR RFOLD NNPRM MJCVI WEIOZ TVRPW LHCCP PKCRT VIIOY REETD UUKRV ESSMT ZRVNR TDWRA BFBIL HCAUH AEVIT CEEWS VIHSI EDDQN DPAWK RLHZR CSTOL DUSJO VEEYV RQWGN TIATG ELUII HHQDQ OVHKI FENUH TQCYM AIASB ONAAE FFKCT LRTVI HSZIH EZBAT PEWEG SFNPL RARNL DYNBO RDVNP TSAHD KCHDW DPOCU OLLAR AKGVR LSBAT LZSQM GNHUH DTRJO VETOA TGEBO LSLRV CWIBN YFLHG SNUVE DPVIE TOIAH VHMUX ELHNF JRWTH EKITA HEPEG EKTKI LAJLD PAWVR ODMAD PNFLT SKQNA TKEFR PWPHZ NVIVI OGGNL HRAEE GPJOL IHEBE GCWLR EVTJO VETFK RLHVI LSLAL LBTBO TOIOE RXEKW BRTOC CZVER MSQNV ERNTA AHLHT LNGAT IOWSN AWDLH VILSL ALLBT BONAP DIAGN RENAG CTOAT PEOIA HVHMU PEMAH PXTMA REOGR NHSGU HPNAJ DREVE TAFOV SAIAG AIHEI OFRJW CABTD VBTWE SAAWU ECUEP HEATF ESAXE LHVHX LWEKG RNRRH TJRAT TAAWG PQOLU HERAM EHDGN KILAH TATGO WEPEG EXTID FOATP EOIAH VHMUW EHOHH LTPPV RPPKR USNAF BGPQO LUHEE EASJL EWATI OKTIA ASTNM SHSKI WIGNL HYYVR UDDIR EZBPA IAFRX FXTMA REOGR NDSQO TAHEH DTEVI ATVEQ OYKHF PLDAB OPHWL FBRNT AAHLH TLNGA TIOWS REOGR NFRAO GMJCT OATRE WOWSN AKDEE MTLHN YFRXD GCAIG MKFPL EDRYL HFRMC SSQNA TTADW KILHU SPAWE AIUML SKARE ZBRNO IIHIO JRTFK RLHJC LUVNT NTOAT PEOIA HVHMU QECAP HRNPR BIWSQ ELSHH GTGOM UXPKC BOZSV AFEIE NNFRM UHEMA BOZGL HZLCC DLDWQ OBOBT HNXEP OPEMI ZVATT AIORR VCDIB IMISS VRGSQ MVHNW PETAH EHTAT PEOIA HVHMU EEQNA TQEVI WEBIA IWABO HDKRP OLEZO WDLHL HVAKY CAPHR NPRQC INVEB TTNAT QEJOQ EREHT YFLHM MKSDI JEJLD WIETL GNHOL HUITP TRTEB TMBQE HOWIB NRFQY XTZRI OWSPL VCZRC CNLHH XRWEA TFRAU AHLHE WVRAS BTVSN INFWC ELHTQ CBMVR LHQNA TTAAT ZERAW IMUPP KRGSY FLHMM JCVIG ENAQC NMVIX EDOFR XDGCN ARYPH TNPAG POOJC VINGG UNAAT IONGT TQNCY BTVTA OJPXT ESFOS EZVKR PHUSA OJPXT VRNMR OMILS LHDVA SBOTO ATGEI OGNWE GSQFJ OPECA XEHHH TFFMM JCVIA EPHJC CCTNP OCEBO PDVEG ELAWC LLBTB OHTAT EENNJ LASGS YFLHV VLSUS VRHEG SNFSN FOMMB TBOAT TABNY WFRKS ATAEF OOLZB ATQEZ ICAPL TOXTP LXGPA FHIOJ RTTKF TAGSN UVEDP VIATV AFEIE NNMBX ESOMX ZLBIA TEEGD BIWIN NPLRN XRNYF RDVHD EDRYL HRNCW REVET AFOLS ROIOM BXELH QCBMN UVIVG KOVET OATEE NNJLB TCCPL TNVIE EQNRA REKOB APLHT GPQOW ELSEW GCSAM MLNXT PLXGM AASGN LHSSQ MBTQM MUVIZ GLHLS TAAIB TCCEL GEUHY IYUHS WIRNV EHEZB BBCBG AEEQN VNNGB TTNLA EEQNI AHDED RYLHD PAWFR YFLHH DDVGC ATQEG OLTGE LUPEC YPNXI LHVRP DWESS KGQSM IOLAT MEJRG EVRLT GPJOL IVIOG GNPET LKGPN FECEB ODDLN ATVIZ GHHVT IOOLF BSAGC KRIAR NEDCY ZVGNL HLLXR WEGTX EKWBR TOGAR ELTDW TIXEP HESWU PHYSC ODDQN HTFBG CPLSB TAHEA TVEME SSDLP ODARN OWIIP MAIPA DPVIX TWDFO TEFOM THHMT LHDAL DBTBO LAJVF LQMTO KTFAN FQCGN LHXTP LXGPA FHRNP TFOKK QNATM EPNVR SAHER RXTTA DWKID HLEVA FEIEN NMBXE ROGEX ILHKR VHWSQ MKSGA WEHHK SKTXE JCHEA TEEJB TLRTT OIODR GOITK OVEZF PLTNV IXELH TFHUD DBTBO MMJCV IXEGO JUDPK RATEE NNJLL SASGN LHZIK WBRSA RAWEF UPTVE AINKF RJWVI OGVNS NFOMM BTBOA WCCAN HOMUA ELEVA VEEEJ CHEAT PEUIB IGSBF HHHTR NMMJC VIGEN ARDNA LDBFE EVRHT MMOKA TQEGO HTYFP HFSAE PBHUR TATRE AWEWW LRNHE HTTLX NWSZV RRTLG NLHZI QCBMN UTAWI FNGSA TTAAT CECCN NNNJL WSNAU DTEFO PTHHM TLHDY VIDDX SSATT LSLHQ MKSGA WEQFK OATAI EWWLV RKQZI EERNB NDRIO ASPNV RSAME BNLUG PROWU BTBOT OPEGE KTKIB IUYPH JCCCA NHOFB CAPHZ EHEZB QCNMM UBTBO EWATI OTTLL XRMEP NVRSA MEGNL HQCFN GUIPW IBNKF YUELL HQCLS KWFUE DTEFR PHJBB TZVVB XTZVV NNFVT IOOLF BQMMT LHQCI RWEDP QNVIO GPNVR SAMEG NLHGP ROWUB TBOTO LHTAN AQDWO WSKWF UEDYE YNSIP ERYSO USPAE EMTRE BTBOZ EREBO ATAET IQDFO ATEEL SQCSA BTBFE EVRHT VRLAA ISELU ZFWLM AJBBT BOOSR EIEWD TOVID DAMVR VRAMX TZLCC BTBOE WATRA EEBDG SLUGC TODPA WVRLD GSFNK WBRTA PAAPX EBOZS KOYSV RTAGP AOAIE EQNBI PHXNS IBNPW LHLIE EWLQO TAWIG NPOCU OLBIM MPRZE VEWEU TCOPL";
    println!("{:?}", cipher::Hill2x2.crack(message));
}
