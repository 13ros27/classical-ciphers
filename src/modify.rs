#![allow(dead_code)]
use crate::analyse;

/// Remove all extra characters from a text
pub fn clean(mes: &str) -> String {
    mes.chars()
        .filter(|c| c.is_alphabetic())
        .collect::<String>()
}

/// Remove all extra characters from a text and show the expansion
pub fn clean_verbose(mes: &str) -> (String, Option<Vec<char>>) {
    let new_mes = clean(mes);
    if mes == new_mes {
        (new_mes, None)
    } else {
        let mut expansion = Vec::new();
        for letter in mes.chars() {
            if letter.is_alphabetic() {
                if letter.is_lowercase() {
                    expansion.push('l');
                } else {
                    expansion.push('c');
                }
            } else {
                expansion.push(letter);
            }
        }
        (new_mes, Some(expansion))
    }
}

/// Expand a text back to normal
pub fn expand(mes: &str, expansion: Option<Vec<char>>) -> String {
    if let Some(expansion) = expansion {
        let mut mes = mes.chars();
        let mut expanded_mes = String::new();
        for letter in expansion {
            if letter == 'l' {
                expanded_mes.push(mes.next().unwrap().to_ascii_lowercase());
            } else if letter == 'c' {
                expanded_mes.push(mes.next().unwrap().to_ascii_uppercase());
            } else {
                expanded_mes.push(letter);
            }
        }
        expanded_mes
    } else {
        mes.to_string()
    }
}

/// Split a text into a vector of strings consisting of every nth character
pub fn split(text: &str, parts: usize) -> Vec<String> {
    (0..parts)
        .map(|index| text.chars().skip(index).step_by(parts).collect::<String>())
        .collect::<Vec<String>>()
}

/// Join a text which has been 'split' back together
pub fn rejoin(texts: Vec<String>) -> String {
    let max_len = texts.iter().map(|t| t.len()).max().unwrap();
    let texts: Vec<String> = texts
        .iter()
        .map(|t| format!("{:¬<1$}", t, max_len))
        .collect();
    let mut text = String::new();
    for i in 0..max_len {
        for one_text in &texts {
            text.push(one_text.chars().nth(i).unwrap());
        }
    }
    text.replace('¬', "")
}

/// Invert a transposition cipher
pub fn invert_transposition(text: &str, kl: usize) -> String {
    let split_length = text.len() / kl;
    rejoin(
        (0..text.len())
            .map(|index| {
                text.chars()
                    .skip(index * split_length)
                    .take(split_length)
                    .collect()
            })
            .collect(),
    )
}

pub fn semi_morse(mes: &str, space: Option<char>) -> String {
    let letters = mes.split(' ').collect::<Vec<_>>();
    let mut available_letters = letters.clone();
    available_letters.sort_unstable();
    available_letters.dedup();
    letters
        .iter()
        .map(|l| {
            if space == Some(l.chars().next().unwrap()) {
                ' '
            } else {
                analyse::ASCII_LOWER[available_letters.iter().position(|al| al == l).unwrap()]
            }
        })
        .collect::<String>()
}

pub fn morse(mes: &str, dot: char, dash: char, space: Option<char>) -> String {
    let mes = mes.replace(dot, ".").replace(dash, "-");
    let letters = mes.split(' ').collect::<Vec<_>>();
    letters
        .iter()
        .map(|l| {
            if space == Some(l.chars().next().unwrap()) {
                ' '
            } else {
                analyse::ASCII_LOWER[analyse::MORSE_LETTERS
                    .iter()
                    .position(|al| al == l)
                    .unwrap()]
            }
        })
        .collect::<String>()
}

pub(crate) fn string_to_num(mes: &str) -> Vec<usize> {
    mes.to_ascii_uppercase()
        .chars()
        .map(|l| l as usize - 65)
        .collect()
}

#[cfg(test)]
mod tests {
    const TEST_TEXT: &str = "Hello how are you, testing, testing, 1 2 3";
    const EXPANSION: [char; 42] = [
        'c', 'l', 'l', 'l', 'l', ' ', 'l', 'l', 'l', ' ', 'l', 'l', 'l', ' ', 'l', 'l', 'l', ',',
        ' ', 'l', 'l', 'l', 'l', 'l', 'l', 'l', ',', ' ', 'l', 'l', 'l', 'l', 'l', 'l', 'l', ',',
        ' ', '1', ' ', '2', ' ', '3',
    ];

    #[test]
    fn clean() {
        assert_eq!(super::clean(TEST_TEXT), "Hellohowareyoutestingtesting");
    }

    #[test]
    fn clean_verbose() {
        assert_eq!(
            super::clean_verbose(TEST_TEXT),
            (
                String::from("Hellohowareyoutestingtesting"),
                Some(EXPANSION.to_vec()),
            )
        );
    }

    #[test]
    fn expand() {
        assert_eq!(
            super::expand("Hellohowareyoutestingtesting", Some(EXPANSION.to_vec())),
            TEST_TEXT
        );
    }

    #[test]
    fn split() {
        assert_eq!(
            super::split(TEST_TEXT, 5),
            vec!["H aoegs, ", "ehrus,t 3", "loe,t i1", "lw  itn ", "o ytneg2"],
        );
    }

    #[test]
    fn rejoin() {
        assert_eq!(
            super::rejoin(vec![
                String::from("H aoegs, "),
                String::from("ehrus,t 3"),
                String::from("loe,t i1"),
                String::from("lw  itn "),
                String::from("o ytneg2")
            ])
            .trim(),
            TEST_TEXT,
        );
    }

    #[test]
    fn invert_transposition() {
        assert_eq!(
            super::invert_transposition(TEST_TEXT, 5),
            "Hwuni e ,gn3la ,glrt ,oeet   se1hyts ooit2",
        );
    }
}
