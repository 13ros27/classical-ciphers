# classical-ciphers

Rust project to implement the automated or semi-automated cracking of classical ciphers.

TODO:<ul>
	<li>Monosubstitution Cipher:<ul>
		<li>Write generic simulated annealing algorithm</li>
	</ul></li>
	<li>Name invert_transposition more generally and write an opposite which joins together a bunch of splits</li>
	<li>Use the new code above to hopefully be able to encrypt and decrypt ColTrans</li>
	<li>Possibly modify clean to allow numbers through when they are the only thing in the cipher or make auto::crack do this instead</li>
	<li>Maybe possible to have some method to at least help fix monoalphabetic cracking by for example suggesting letters that aren't used and similar</li>
	<li>Maybe add offset to cracking Railfence</li>
	<li>Start monoalphabetic hill-climbing with the letters given by ioc</li>
	<li>Add decrypt and encrypt for Polybius</li>
</ul>
