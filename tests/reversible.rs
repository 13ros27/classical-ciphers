use classical_ciphers::{
    cipher::{Affine, Beaufort, Caesar, MonoSubstitution, Permutation, Vigenere},
    prelude::*,
};
use rand::prelude::*;

const TEST_TEXT: &str = "Hello how are you, testing, testing, 1 2 3";

#[test]
fn caesar() {
    let mut rng = rand::thread_rng();
    let keyspace = Caesar.keyspace();
    let key = keyspace.choose(&mut rng).unwrap();
    assert_eq!(
        Caesar.decrypt(&Caesar.encrypt(TEST_TEXT, &key).unwrap(), &key),
        Some(TEST_TEXT.to_string()),
    );
}

#[test]
fn affine() {
    let mut rng = rand::thread_rng();
    let keyspace = Affine.keyspace();
    let key = keyspace.choose(&mut rng).unwrap();
    assert_eq!(
        Affine.decrypt(&Affine.encrypt(TEST_TEXT, &key).unwrap(), &key),
        Some(TEST_TEXT.to_string()),
    );
}

#[test]
fn vigenere() {
    let mut rng = rand::thread_rng();
    let key_len = 4 + (rng.gen::<f64>() * 8.0) as usize;
    let mut key = String::new();
    for _ in 0..key_len {
        key.push((rng.gen::<f64>() * 26.0 + 65.0) as u8 as char);
    }
    assert_eq!(
        Vigenere.decrypt(
            &Vigenere.encrypt(TEST_TEXT, &key.to_owned()).unwrap(),
            &key.to_owned()
        ),
        Some(TEST_TEXT.to_string()),
    );
}

#[test]
fn beaufort() {
    let mut rng = rand::thread_rng();
    let key_len = 4 + (rng.gen::<f64>() * 8.0) as usize;
    let mut key = String::new();
    for _ in 0..key_len {
        key.push((rng.gen::<f64>() * 26.0 + 65.0) as u8 as char);
    }
    assert_eq!(
        Beaufort.decrypt(
            &Beaufort.encrypt(TEST_TEXT, &key.to_owned()).unwrap(),
            &key.to_owned()
        ),
        Some(TEST_TEXT.to_string()),
    );
}

#[test]
fn permutation() {
    const CUSTOM_TEST: &str = "Hello how are you, testing, testing, 1 2 3xx";
    let mut rng = rand::thread_rng();
    let key_choices = vec![5, 6, 10];
    let key_len = key_choices.choose(&mut rng).unwrap();
    let mut key: Vec<usize> = (0..*key_len).collect();
    key.shuffle(&mut rng);
    assert_eq!(
        Permutation.decrypt(&Permutation.encrypt(CUSTOM_TEST, &key).unwrap(), &key),
        Some(CUSTOM_TEST.to_string()),
    );
}

#[test]
fn mono_substitution() {
    const CUSTOM_TEST: &str = "Hello how are you, testing, testing, 1 2 3xx";
    let mut rng = rand::thread_rng();
    let mut key: Vec<usize> = (0..26).collect();
    key.shuffle(&mut rng);
    assert_eq!(
        MonoSubstitution.decrypt(&MonoSubstitution.encrypt(CUSTOM_TEST, &key).unwrap(), &key),
        Some(CUSTOM_TEST.to_string()),
    );
}
